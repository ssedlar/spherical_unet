#! /usr/bin/env python

"""Generates synthetic dMRI signals based on ball and stick model [1] from HCP dataset."""

# [1] Wilkins, Bryce, et al. "Fiber estimation and tractography in diffusion MRI:
# development of simulated brain images and comparison of multi-fiber analysis methods at clinical b-values."
# Neuroimage 109 (2015): 341-356.

import sys
import os
import argparse

from sph_unet.load_config_file import load_config_file


def main():

    parser = argparse.ArgumentParser(description='Synthetic dMRI HCP database generation.')
    parser.add_argument('-db_in', dest='db_in', required=True, help='Path to the input dataset.')
    parser.add_argument('-config', dest='config_path', required=True, help='Path to the configuration file')
    parser.add_argument('-subject_idx', dest='subject_idx', type=int, required=True, help='Name of the subject')

    args = parser.parse_args()

    # 1. Loading configuration file
    modules_objects = load_config_file(args.config_path)

    # 2. Setting up database path and loading subjects list
    db = modules_objects['database']
    db.set_database_path(args.db_in)
    db.set_dmri_template_path()
    db.load_subjects()

    db.generate_data(args.subject_idx)

    print(db.subjects_list)


if __name__ == '__main__':
    main()
