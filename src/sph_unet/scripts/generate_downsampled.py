#! /usr/bin/env python
"""Generates downsampled dMRI signals for HCP dataset."""

import sys
import os
import argparse

from sph_unet.load_config_file import load_config_file


def main():

    parser = argparse.ArgumentParser(description='Downsampling dMRI HCP database.')
    parser.add_argument('-db_in', dest='db_in', required=True, help='Path to the input dataset.')
    parser.add_argument('-config', dest='config_path', required=True, help='Path to configuration file')

    args = parser.parse_args()

    # 1. Loading configuration file
    modules_objects = load_config_file(args.config_path)

    # 2. Setting up database path and loading subjects list
    db = modules_objects['database']
    db.set_database_path(args.db_in)
    db.set_dmri_template_path()
    db.load_subjects()

    # 3.
    prep = modules_objects['preprocessor']
    prep.downsample_data(db)


if __name__ == '__main__':
    main()
