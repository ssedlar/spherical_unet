#! /usr/bin/env python
"""Script that runs FOD estimator's training (and validation) or testing."""
import sys
import os
import argparse

from sph_unet.load_config_file import load_config_file


def main():
    """Function that runs FOD estimator's training (and validation) or testing."""

    parser = argparse.ArgumentParser(description='Model training and validation.')
    parser.add_argument('-config', dest='config_path', required=True, help='Path to the configuration file.')
    parser.add_argument('-db_in', dest='db_in', required=True, help='Path to the input dataset.')
    parser.add_argument('-exp_out', dest='exp_out', required=True, help='Path to the experiment output.')

    args = parser.parse_args()

    if not os.path.exists(args.config_path):
        print("\nConfiguration file does not exist!\n")
        sys.exit(2)

    # 1. Loading configuration file
    modules_objects = load_config_file(args.config_path)

    # 2. Setting up database path and loading subjects list
    db = modules_objects['database']
    db.set_database_path(args.db_in)
    db.set_dmri_template_path()
    db.load_subjects()

    # 3. Setting up preprocessor and preparing data for the estimator
    prep = modules_objects['preprocessor']
    prep.set_exp_out_path(args.exp_out)
    prep.set_preprocessors_source_dir('/'.join(os.path.abspath(__file__).split('/')[:-3]))
    prep.generalize_data_names(db)

    prep.prepare_data(db)
    prep.prepare_ground_truth(db)
    prep.load_info(db)

    # 4. Setting up estimator, data preparation and running training and validation
    estim = modules_objects['estimator']
    estim.set_exp_out_path(args.exp_out)
    estim.set_estimators_source_dir('/'.join(os.path.abspath(__file__).split('/')[:-3]))

    # 5. Estimator's training (and validation) or testing
    estim.train_and_valid(db, prep)


if __name__ == '__main__':
    main()
