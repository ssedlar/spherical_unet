from setuptools import setup, find_packages

setup(
    name='sph_unet',
    version='0.0.0',
    packages=find_packages(),
    url='https://gitlab.inria.fr/SaraSedlar',
    license='MIT',
    author='ATHENA Team, Inria',
    author_email='',
    description='A Python package that implements spherical unet'
                'for the estimation of fODFs from diffusion MRI.'
)
