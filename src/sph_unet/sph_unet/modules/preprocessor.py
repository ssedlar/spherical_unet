"""Class for diffusion MRI data preprocessing and preparation
    for the estimator."""

import os
import numpy as np


class Preprocessor(object):
    """Class for diffusion MRI data preprocessing and preparation
        for the estimator."""

    """
        Attributes:
            exp_out (str): path to the experiment output
            preproc_source_dir (str): preprocessors' source directory
            
            signal_domain (str): signal domain that will be used as input
                to the estimator; options: raw/sph
            gt_type (str): ground truth to be estimated
            
            n_in_len (int): length of input data
            n_in_ch (int): number of channels of input data
            n_out_len (int): length of ground truth
            n_out_ch (int): number of channels of ground truth
            
        Methods:
            set_exp_out_path: sets path to the experiment output
            get_exp_out_path: gets path to the experiment output
            set_preprocessors_source_dir: set path to the preprocessors'
                source directory
            get_preprocessors_source_dir: get path to the preprocessors'
                source directory
            normalize_data: normalizes input dMRI data with average b0 and
                optionally perform mean-std normalization over voxels
            name: returns object's name
    """
    def __init__(self, exp_out=None, signal_domain='sph', gt_type='wm_fod',):

        self.exp_out = exp_out
        self.preproc_source_dir = None

        self.signal_domain = signal_domain
        self.gt_type = gt_type

        self.n_in_len = None
        self.n_in_ch = None
        self.n_out_len = None
        self.n_out_ch = None

    def get_exp_out_path(self):
        """Gets experiment output path."""
        """
            Returns:
                (str): path to the experiment output
        """
        return self.exp_out

    def set_exp_out_path(self, exp_out):
        """Sets experiment output path."""
        """
            Arguments:
                exp_out_path (str): path to the experiment output
        """
        self.exp_out = exp_out

    def get_preprocessors_source_dir(self):
        """Gets preprocessors' source directory path."""
        """
            Returns:
                (str): preprocessors' directory path
        """
        return self.preproc_source_dir

    def set_preprocessors_source_dir(self, source_dir):
        """Sets preprocessors' source directory path."""
        """
            Arguments:
                source_dir (str): entire module's source directory path
        """
        self.preproc_source_dir = os.path.join(source_dir,
                                               'sphcnn', 'modules',
                                               'preprocessors')

    @staticmethod
    def normalize_data(prt, data):
        """Normalizes input dMRI data with b0."""
        """
            Arguments:
                prt (Protocol instance): object containing acquisition
                    information
                data (numpy array): 2D numpy array; each row is one
                    sample/voxel; each column one acquisition point
            Returns:
                (numpy array): 2D array with normalized dMRI;
                    each row is one sample/voxel; each column one acquisition
                    point
        """
        data_b0 = np.expand_dims(np.mean(data[:, prt.gradient_scheme.b0s_mask],
                                         axis=1),
                                 axis=1)
        data = data[:, :] / (data_b0 + 1.e-10)
        data = np.clip(data, 0, 1)
        print(np.sum(np.isnan(data)))
        return data

    @property
    def name(self):
        """Returns object's name."""
        raise NotImplementedError()
