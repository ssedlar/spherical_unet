"""Class for real dMRI data preprocessing and preparing; child class of Preprocessor."""

from .. import Preprocessor

import sys
import os
import numpy as np
import nibabel as nib
from collections import OrderedDict
import shutil


class PreprocessorDownsample(Preprocessor):
    """Class for dMRI data downsampling."""

    """
        Attributes:
            n_shells (int): number of shells
            n_points (int): number of points

        Methods:
            downsample_data: runs downsampling of dMRI data, bvalues and bvectors
    """
    def __init__(self, n_shells=3, n_points=20, **kwargs):
        """Attribute initialization of the class PreprocessorReal."""
        super(PreprocessorDownsample, self).__init__(**kwargs)

        self.n_shells = n_shells
        self.n_points = n_points

    def _load_data_scan(self, db, data_path):
        """Loads dMRI data, mask and protocol of one scan;
            filters out erroneous voxels with low mean b0 values from
            brain mask"""
        """
            Arguments:
                db (Database instance): database object
                data_path (str): path to the scan's dMRI data and protocol
            Returns:
                (4D numpy array, 3D numpy array, Protocol instance):
                    dMRI data, filtered brain mask and acquisition protocol
                    all of one scan
        """
        data_nib = nib.load(os.path.join(data_path, db.data_name + '.nii.gz'))
        prt = db.load_protocol(data_path)
        return data_nib, prt

    def downsample_data(self, db):

        output_path_template = db.dmri_template_path + \
            '(n_shells={},n_points={})'.format(self.n_shells, self.n_points)

        for s in db.subjects_list:

            data_nib, prt =\
                self._load_data_scan(db, db.dmri_template_path.format(s))

            new_header = data_nib.header.copy()
            new_affine = data_nib.affine.copy()

            new_header['dim'][4] = np.sum(prt.gradient_scheme.b0s_mask) + self.n_points
            select = list(np.where(prt.gradient_scheme.b0s_mask)[0]) + \
                     list(np.where(~prt.gradient_scheme.b0s_mask)[0][0:self.n_points])
            select = np.asarray(sorted(select))

            data_new = np.asarray(data_nib.dataobj)[:, :, :, select]

            data_new_nib = nib.Nifti1Image(data_new, affine=new_affine, header=new_header)

            output_path_new = output_path_template.format(s)

            data_new_path = os.path.join(output_path_new, db.data_name + '.nii.gz')
            bval_new_path = os.path.join(output_path_new, 'bvals')
            bvecs_new_path = os.path.join(output_path_new, 'bvecs')

            if not os.path.exists(output_path_new):
                os.makedirs(output_path_new)

            nib.save(data_new_nib, data_new_path)
            np.savetxt(bvecs_new_path,
                       prt.gradient_scheme.bvecs.T[:, select], fmt='%.6f')
            np.savetxt(bval_new_path,
                       np.reshape(prt.gradient_scheme.bvals[select], [1, -1]), fmt='%1d')

            five_tissues_src = os.path.join(db.dmri_template_path.format(s),
                                            '5ttgen.nii.gz')
            nodif_brain_mask_src = os.path.join(db.dmri_template_path.format(s),
                                                'nodif_brain_mask.nii.gz')

            five_tissues_dst = os.path.join(output_path_template.format(s),
                                            '5ttgen.nii.gz')
            nodif_brain_mask_dst = os.path.join(output_path_template.format(s),
                                                'nodif_brain_mask.nii.gz')

            shutil.copy(five_tissues_src, five_tissues_dst)
            shutil.copy(nodif_brain_mask_src, nodif_brain_mask_dst)







