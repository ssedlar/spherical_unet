####################################################################################
# This is a gradient scheme for diffusion MRI acquisition, generated from the
# online Q space sampling script at
# http://www.emmanuelcaruyer.com/q-space-sampling.php. Use this
# gradient scheme freely for your simulations, or real acquisition on an MR
# scanner. Do not forget to cite the relevant article [1].
#
# 1. Caruyer, Emmanuel, Christophe Lenglet, Guillermo Sapiro, and Rachid Deriche.
#    "Design of multishell sampling schemes with uniform coverage in diffusion MRI."
#    Magnetic Resonance in Medicine 69, no. 6 (2013): 1534-1540.
#
####################################################################################
# THERE IS NO WARRANTY FOR THIS SAMPLING SCHEME, TO THE EXTENT PERMITTED BY        #
# APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS    #
# AND/OR OTHER PARTIES PROVIDE THE SAMPLING SCHEME “AS IS” WITHOUT WARRANTY OF     #
# ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE        #
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE  #
# ENTIRE RISK AS TO THE QUALITY OF THE SAMPLING SCHEME IS WITH YOU. SHOULD THE     #
# SAMPLING SCHEME PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, #
# REPAIR OR CORRECTION.                                                            #
####################################################################################
#shell	u_x	u_y	u_z
1	0.061	0.958	0.281
1	-0.989	0.096	-0.114
1	-0.132	-0.272	0.953
1	-0.616	0.448	0.648
1	-0.647	-0.711	0.276
1	-0.569	-0.182	-0.802
1	-0.453	0.788	-0.417
1	-0.023	-0.540	-0.841
1	0.826	0.183	-0.533
1	0.096	0.850	-0.518
1	0.754	0.554	0.353
1	0.714	-0.694	-0.088
1	0.523	-0.356	0.775
1	0.409	-0.095	-0.908
1	0.451	0.883	0.131
1	-0.836	0.375	-0.402
1	0.352	-0.827	-0.438
1	0.468	0.526	-0.710
1	0.948	0.315	-0.038
1	0.172	0.098	0.980
1	0.181	-0.977	0.109
1	0.907	-0.270	-0.324
1	-0.406	-0.653	-0.640
1	0.148	-0.589	0.795
1	-0.863	-0.111	-0.494
1	-0.300	0.576	0.761
1	0.279	0.936	-0.215
1	-0.598	-0.168	0.784
1	0.724	-0.649	0.235
1	0.787	0.617	0.017
