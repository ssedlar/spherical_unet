"""Class for real dMRI data preprocessing and preparing; child class of Preprocessor."""

from .. import Preprocessor

import sys
import os
import numpy as np
import nibabel as nib
from ..estimators.spherical_cnn_utils import get_s2_signal_points
from ..estimators.spherical_cnn_utils import s2_to_sph_basis
from collections import OrderedDict
from scipy.ndimage.morphology import binary_dilation
from scipy import ndimage


class Preprocessor3DCNN(Preprocessor):
    """Class for real dMRI data preprocessing and preparing; child class of Preprocessor."""

    """
        Attributes:
            n_in_len (int): length of input data
            n_in_ch (int): number of channels of input data
            n_out_len (int): length of ground truth
            n_out_ch (int): number of channels of ground truth

            data_list (list[int]): list of all file indices
            train_list (list[int]): list of training file indices
            valid_list (list[int]): list of validation file indices
            test_list (list[int]): list of testing file indices

            sph_ord (int): spherical harmonic order
            n_sph (int): number of spherical harmonic basis
            sph_type (str): type of spherical harmonic basis; options: 'real'/'complex'
            data_type (numpy data type): type of spherical harmonic basis and its coefficients;
                options: 'np.float32'/'np.complex64'

            b0_th (float): mean b0 value threshold for voxel filtering out

            downsample (bool): flag indicating whether to downsample the data
            n_shells (int): number of shells in downsampled data
            n_points (int): number of sampling points in downsampled data

        Methods:
            generalize_data_names: converts database specific naming into more general one
            load_info: loads info about input data or ground truth
            prepare_data_raw: prepares raw data for the estimator
            prepare_data_sph: prepares spherical harmonics for the estimator
            prepare_ground_truth: prepares ground truth for the estimator
    """
    def __init__(self, n_shells=3, n_points='all', b0_th=100, mask_type='white_matter', **kwargs):
        """Attribute initialization of the class PreprocessorReal."""
        super(Preprocessor3DCNN, self).__init__(**kwargs)

        self.n_shells = n_shells
        self.n_points = n_points

        self.n_in_len = None
        self.n_in_ch = None
        self.n_out_len = None
        self.n_out_ch = None

        self.data_list = []
        self.train_list = []
        self.valid_list = []
        self.test_list = []
        self.mask_type = mask_type

        self.sph_ord = None
        self.n_sph = None

        self.b0_th = b0_th

    def _get_spherical_harmonic_max_order(self, s2_coord):

        max_sampling_points = max([s2_coord[k].shape[0] for k in s2_coord])

        sph_ord = 0
        while (sph_ord + 1) * (sph_ord + 2) / 2 <= max_sampling_points:
            sph_ord += 2
        self.sph_ord = sph_ord - 2

        self.n_sph = np.sum([2 * i + 1 for i in range(0, self.sph_ord + 1, 2)])

    def _filter_brain_mask(self, data, prt, mask):
        b0_mean = np.mean(data[:, :, :, prt.gradient_scheme.b0s_mask], axis=3)
        return mask * (b0_mean > self.b0_th)

    def _normalize_data_subj(self, data, prt, mask):
        data_n = self.normalize_data(prt, data[mask == 1, :])
        data_n_shell = OrderedDict()
        for sh in prt.shell_masks:
            data_n_shell[sh] = data_n[:, prt.shell_masks[sh]]
        return data_n_shell

    def _load_data(self, db, data_path, subject):
        data = np.asarray(nib.load(os.path.join(data_path, db.data_name + '.nii.gz')).dataobj)
        if self.mask_type == 'whole_brain':
            mask = np.array(nib.load(os.path.join(data_path, 'nodif_brain_mask.nii.gz')).dataobj).astype(np.int32)
        elif self.mask_type == 'white_matter':
            tissue_types_path = db.get_dmri_template_path().format(subject)
            tissues = nib.load(os.path.join(tissue_types_path, '5ttgen.nii.gz')).get_data()[::-1]
            mask = np.argmax(tissues, axis=3) == 2

        prt = db.load_protocol(data_path)
        mask = self._filter_brain_mask(data, prt, mask)
        return data, mask, prt

    def _load_ground_truth(self, gt_path, mask):
        gt = np.asarray(nib.load(os.path.join(gt_path, self.gt_type + '.nii.gz')).dataobj)
        if len(gt.shape) == 3:
            gt = gt[mask == 1]
        else:
            gt = gt[mask == 1, :]
        if len(gt.shape) == 1:
            gt = np.expand_dims(gt, axis=1)
        if len(gt.shape) == 2:
            gt = np.expand_dims(gt, axis=2)
        return gt

    def generalize_data_names(self, db):
        """Change database specific naming into more general one."""
        """
            Arguments:
                db (Database): database object
        """
        for s_idx, s in enumerate(db.subjects_list):
            self.data_list.append(s_idx)
            if s in db.train_subjects:
                self.train_list.append(s_idx)
            elif s in db.valid_subjects:
                self.valid_list.append(s_idx)
            elif s in db.test_subjects:
                self.test_list.append(s_idx)

    def load_info(self, db):
        """Load info about input and ground truth dimensions and path."""
        """
            db (Database): database object
            prep (Preprocessor): preprocessor object
        """
        signal_info_path = os.path.join(self.exp_out, db.name, self.name(), 'data_info')
        with open(signal_info_path, 'r') as f:
            info_str = str.split(f.readline(), ' ')
            info = [int(i) for i in info_str[:-1]]
        self.n_in_len, self.n_in_ch = info[0:2]

        n_shells = info[2]
        self.n_sph = self.n_in_len / n_shells
        sph_ord, sph_c = 0, 0
        while sph_c != self.n_sph:
            sph_c += (2 * sph_ord + 1)
            sph_ord += 2
        self.sph_ord = sph_ord - 2

        gt_info_path = os.path.join(self.exp_out, db.name, 'ground_truth', self.gt_type + '_info')
        with open(gt_info_path, 'r') as f:
            info_str = str.split(f.readline(), ' ')
            info = [int(i) for i in info_str[:-1]]
            self.n_out_len, self.n_out_ch = info[0:2]

    def prepare_data(self, db):
        """Prepare spherical harmonic input data for the estimator."""
        """
            Arguments:
                db (Database): Database object
        """
        # 1. Defining templates for the raw input and prepared output sph data paths

        if self.n_points != 'all':
            input_template_path = db.get_dmri_template_path() + \
                                  '(n_shells={},n_points={})'.format(self.n_shells, self.n_points)
        else:
            input_template_path = db.get_dmri_template_path()
        output_template_path = os.path.join(self.exp_out, db.name, self.name(), 'data_{}.npy')
        n_vox_template_path = os.path.join(self.exp_out, db.name, self.name(), 'n_vox_{}')
        mask_shifts = []
        for i in range(-1, 2):
            for j in range(-1, 2):
                for k in range(-1, 2):
                    mask_shifts.append([i, j, k])

        # 2. Normalizing dMRI with b0 and conversion to spherical harmonics per subject
        for s in self.data_list:
            # 2.1. Get data output path, check if it is already computed
            s_output_path = output_template_path.format(str(s))
            if os.path.exists(s_output_path):
                continue
            if not os.path.exists(os.path.dirname(s_output_path)):
                os.makedirs(os.path.dirname(s_output_path))

            # 2.2. Loading raw dMRI, brain mask and protocol
            data, mask, prt = self._load_data(db, input_template_path.format(db.subjects_list[s]), db.subjects_list[s])

            dilation_structure = ndimage.generate_binary_structure(3, 3)
            mask_dilated = binary_dilation(mask, dilation_structure)

            # 2.3. Normalizing data with b0
            data_n_shells = self._normalize_data_subj(data, prt, mask_dilated)

            # 2.4. Computing spherical harmonic basis
            signal_s2_coord = get_s2_signal_points(prt)

            self._get_spherical_harmonic_max_order(signal_s2_coord)

            s2signal2sph = OrderedDict()
            for shell in signal_s2_coord:
                s2signal2sph[shell] = s2_to_sph_basis(signal_s2_coord[shell][:, 1:],
                                                      self.sph_ord,
                                                      sph_type='real', normalize=True, sph_inv='pinv')

            # 2.5. Allocating memory for spherical harmonics
            n_shells, n_samples = len(data_n_shells.keys()), int(np.sum(mask))

            h, w, d = mask.shape
            data_sph = np.zeros((h, w, d, self.n_sph, n_shells), dtype=np.float32)
            data_sph_nb = np.zeros((n_samples, self.n_sph * n_shells, 27), dtype=np.float32)
            bm_idx = np.where(mask)

            # 2.6. Computing spherical harmonic coefficients
            for sh_idx, sh in enumerate(data_n_shells):
                data_sph[mask_dilated == 1, :, sh_idx] = np.dot(data_n_shells[sh], s2signal2sph[sh].transpose())

            for sh_idx in range(n_shells):
                for m_idx, m_shift in enumerate(mask_shifts):
                    x_sh, y_sh, z_sh = m_shift
                    data_sph_nb[:, sh_idx * self.n_sph:(sh_idx + 1) * self.n_sph, m_idx] = \
                        data_sph[bm_idx[0] + x_sh, bm_idx[1] + y_sh, bm_idx[2] + z_sh, :, sh_idx]

            # 2.7. Check compatibility of input data
            _, n_in_len, n_in_ch = data_sph_nb.shape
            if not self.n_in_len:
                self.n_in_len, self.n_in_ch = n_in_len, n_in_ch
            else:
                if self.n_in_len != n_in_len or self.n_in_ch != n_in_ch:
                    raise ValueError('Input data not compatible!')

            # 2.8. Saving data
            np.save(s_output_path, data_sph_nb)
            with open(n_vox_template_path.format(str(s)), 'w+') as f:
                f.write('%d' % n_samples)

            # 2.9. Print progress in data preparation
            sys.stdout.write("\rPreparing sph data (b0 normalization, sph computation): "
                             "%.3f %% / 100.00 %%" % (100 * float(s + 1) / db.n_subjects))
            sys.stdout.flush()
        sys.stdout.write("\n")

        # 3. Saving information about input and ground truth data dimensions
        output_info_path = os.path.join(self.exp_out, db.name, self.name(), 'data_info')

        if not os.path.exists(output_info_path):
            with open(output_info_path, 'w+') as f:
                f.write('%d %d %d ' % (self.n_in_len, self.n_in_ch, n_shells))

    def prepare_ground_truth(self, db):
        """Prepare ground truth data."""
        """
            Arguments:
                db (Database): Database object
        """
        # 1. Defining templates for the raw input and prepared output data paths
        input_template_path = db.get_dmri_template_path()
        output_template_path = os.path.join(self.exp_out, db.name, 'ground_truth', '{}_{}.npy')

        output_info_path = os.path.join(self.exp_out, db.name, 'ground_truth', self.gt_type + '_info')
        if os.path.exists(output_info_path):
            return

        for s in self.data_list:
            # 2.1. Get data output path, check if it is already computed
            s_output_path = output_template_path.format(self.gt_type, str(s))
            if os.path.exists(s_output_path):
                continue
            if not os.path.exists(os.path.dirname(s_output_path)):
                os.makedirs(os.path.dirname(s_output_path))

            # 2.2. Loading raw dMRI, brain mask and protocol
            data, mask, prt = self._load_data(db, input_template_path.format(db.subjects_list[s]), db.subjects_list[s])

            # 2.3. Preparing ground truth
            gt = self._load_ground_truth(input_template_path.format(db.subjects_list[s]), mask)

            # 2.4. Check compatibility of ground truth data
            _, n_out_len, n_out_ch = gt.shape
            if not self.n_out_len:
                self.n_out_len, self.n_out_ch = n_out_len, n_out_ch
            else:
                if self.n_out_len != n_out_len or self.n_out_ch != n_out_ch:
                    raise ValueError('Ground truth data not compatible!')

            # 2.5. Saving ground truth data
            np.save(s_output_path, gt)

            # 2.6. Print progress in data preparation
            sys.stdout.write("\rPreparing ground truth: %.3f %% / 100.00 %%" % (100 * float(s + 1) / db.n_subjects))
            sys.stdout.flush()
        sys.stdout.write("\n")

        # 3. Saving information about input and ground truth data dimensions
        output_info_path = os.path.join(self.exp_out, db.name, 'ground_truth', self.gt_type + '_info')
        if not os.path.exists(output_info_path):
            with open(output_info_path, 'w+') as f:
                f.write('%d %d ' % (self.n_out_len, self.n_out_ch))

    def name(self):
        """Return object's name including parameters."""
        return ("%s(n_shells=%s, n_points=%s)"
                % (type(self).__name__, self.n_shells, self.n_points))


