"""Class for dMRI descriptor (micro-structure parameters, fODF) estimation using spherical CNN."""

import sys
import os

import numpy as np
import numpy.matlib

import time

import tensorflow as tf
from .. import Estimator

from .nn_utils import weight_variable
from .nn_utils import weights_orthogonal


import matplotlib.pyplot as plt


class EstimatorCNN3D_fod(Estimator):
    """Class for dMRI descriptor (micro-structure parameters, fODF) estimation using spherical CNN."""
    """
        Attributes:
            n_epochs (int): number of training and validation epochs
            batch_size (int): number of samples per batch
            lr (float): learning rate
            w_std (float): standard deviation of weights for initialization
            activ_func (tf function): activation function (non-linearity)

            ch_o (list[int]): number of channels at the output of each layer

        Methods:
            set_exp_out: setting experiment output path
            train_and_valid: runs training and validation of the model
            test: runs testing of the model
            save_model: method for model saving
            restore_model: method for model restoring
            name: returns object's name including parameters
    """

    def __init__(self, n_epochs=100, batch_size=256, lr=1.e-3, w_std=0.1, activ_func=tf.nn.relu,
                 **kwargs):
        super(EstimatorCNN3D_fod, self).__init__(**kwargs)

        # General attributes for neural networks
        self.n_epochs = n_epochs
        self.batch_size = batch_size
        self.lr = lr
        self.w_std = w_std
        self.activ_func = activ_func

        self.sess = tf.Session()

    def _define_model(self, prep):

        # 1. Allocating memory for input spherical harmonics, ground truth and learning rate
        self.y_sph = tf.placeholder(tf.float32, shape=[None, prep.n_in_len, prep.n_in_ch])
        self.gt = tf.placeholder(tf.float32, shape=[None, prep.n_out_len, prep.n_out_ch])
        self.lr_tf = tf.placeholder(tf.float32)

        v1_idx = np.asarray([0, 1, 3, 4, 9, 10, 12, 13])
        v2_idx = np.asarray([3, 4, 6, 7, 12, 13, 15, 16])
        v3_idx = np.asarray([9, 10, 12, 13, 18, 19, 21, 22])
        v4_idx = np.asarray([12, 13, 15, 16, 21, 22, 24, 25])
        v5_idx = np.asarray([1, 2, 4, 5, 10, 11, 13, 14])
        v6_idx = np.asarray([4, 5, 7, 8, 13, 14, 16, 17])
        v7_idx = np.asarray([10, 11, 13, 14, 19, 20, 22, 23])
        v8_idx = np.asarray([13, 14, 16, 17, 22, 23, 25, 26])

        y_sph_v1 = tf.reshape(tf.gather(self.y_sph, v1_idx, axis=2), shape=[-1, prep.n_in_len * 8])
        y_sph_v2 = tf.reshape(tf.gather(self.y_sph, v2_idx, axis=2), shape=[-1, prep.n_in_len * 8])
        y_sph_v3 = tf.reshape(tf.gather(self.y_sph, v3_idx, axis=2), shape=[-1, prep.n_in_len * 8])
        y_sph_v4 = tf.reshape(tf.gather(self.y_sph, v4_idx, axis=2), shape=[-1, prep.n_in_len * 8])
        y_sph_v5 = tf.reshape(tf.gather(self.y_sph, v5_idx, axis=2), shape=[-1, prep.n_in_len * 8])
        y_sph_v6 = tf.reshape(tf.gather(self.y_sph, v6_idx, axis=2), shape=[-1, prep.n_in_len * 8])
        y_sph_v7 = tf.reshape(tf.gather(self.y_sph, v7_idx, axis=2), shape=[-1, prep.n_in_len * 8])
        y_sph_v8 = tf.reshape(tf.gather(self.y_sph, v8_idx, axis=2), shape=[-1, prep.n_in_len * 8])

        weights_1 = weights_orthogonal('W1', shape=[prep.n_in_len * 8, 1024])
        bias_1 = weight_variable('b1', shape=[1024], std=self.w_std)
        y1_v1 = self.activ_func(tf.matmul(y_sph_v1, weights_1) + bias_1)
        y1_v2 = self.activ_func(tf.matmul(y_sph_v2, weights_1) + bias_1)
        y1_v3 = self.activ_func(tf.matmul(y_sph_v3, weights_1) + bias_1)
        y1_v4 = self.activ_func(tf.matmul(y_sph_v4, weights_1) + bias_1)
        y1_v5 = self.activ_func(tf.matmul(y_sph_v5, weights_1) + bias_1)
        y1_v6 = self.activ_func(tf.matmul(y_sph_v6, weights_1) + bias_1)
        y1_v7 = self.activ_func(tf.matmul(y_sph_v7, weights_1) + bias_1)
        y1_v8 = self.activ_func(tf.matmul(y_sph_v8, weights_1) + bias_1)

        weights_2 = weights_orthogonal('W2', shape=[8 * 1024, 512])
        bias_2 = weight_variable('b2', shape=[512], std=self.w_std)
        y2 = self.activ_func(tf.matmul(tf.concat([y1_v1, y1_v2, y1_v3, y1_v4, y1_v5, y1_v6, y1_v7, y1_v8], axis=1),
                                       weights_2) + bias_2)

        weights_3 = weights_orthogonal('W3', shape=[512, 512])
        bias_3 = weight_variable('b3', shape=[512], std=self.w_std)
        y3 = self.activ_func(tf.matmul(y2, weights_3) + bias_3)

        weights_4 = weights_orthogonal('W4', shape=[512, 256])
        bias_4 = weight_variable('b4', shape=[256], std=self.w_std)
        y4 = self.activ_func(tf.matmul(y3, weights_4) + bias_4)

        weights_5 = weights_orthogonal('W5', shape=[256, prep.n_out_len])
        bias_5 = weight_variable('b5', shape=[prep.n_out_len], std=self.w_std)
        y5 = tf.matmul(y4, weights_5) + bias_5

        # 5. Computing errors, minimization of loss
        self.fod_estim = tf.expand_dims(y5, axis=2)
        self.estims_mse = tf.squeeze(tf.reduce_sum(tf.pow(self.gt - self.fod_estim, 2.), axis=1), axis=1)
        self.mse = tf.reduce_mean(self.estims_mse)
        self.min_loss = tf.train.AdamOptimizer(self.lr_tf).minimize(self.mse)

        self.sess.run(tf.global_variables_initializer())
        self.saver = tf.train.Saver(tf.trainable_variables(), max_to_keep=100000)

    def _train_step(self, data_batch, gt_batch):
        self.sess.run(self.min_loss, feed_dict={self.y_sph: data_batch, self.gt: gt_batch,
                                                self.lr_tf: self.lr})

    def _train_model(self, train_data, train_gt):
        count = 0
        n_train = train_data.shape[0]
        while count < n_train:
            data_batch = train_data[count:count + self.batch_size, :, :]
            gt_batch = train_gt[count:count + self.batch_size, :, :]
            count += self.batch_size
            self._train_step(data_batch, gt_batch)

    def _valid_step_mse(self, data_batch, gt_batch):
        return self.sess.run(self.estims_mse, feed_dict={self.y_sph: data_batch, self.gt: gt_batch})

    def _valid_estims(self, data_batch):
        return self.sess.run(self.fod_estim, feed_dict={self.y_sph: data_batch})

    def _valid_model(self, data, gt, it):

        err_mse = np.zeros(gt.shape[0])
        n_data = data.shape[0]
        count = 0
        while count < n_data:
            data_batch = data[count:count + self.batch_size, :, :]
            gt_batch = gt[count:count + self.batch_size, :, :]
            err_mse[count:count + self.batch_size] = self._valid_step_mse(data_batch, gt_batch)
            count += self.batch_size

        return np.mean(err_mse)

    def _test_model(self, data, gt):

        estims = np.zeros_like(gt)
        n_data = data.shape[0]
        count = 0
        while count < n_data:
            data_batch = data[count:count + self.batch_size, :, :]
            estims[count:count + self.batch_size, :, :] = self._valid_estims(data_batch)
            count += self.batch_size
        return estims

    def _load_data(self, db, prep, data_list):

        count = 0
        for f in data_list:
            n_vox_path = os.path.join(prep.exp_out, db.name, prep.name(), 'n_vox_' + str(f))
            with open(n_vox_path, 'r') as f_vox:
                count += int(f_vox.readline())

        data = np.zeros((count, prep.n_in_len, prep.n_in_ch), dtype=np.float32)
        gt = np.zeros((count, prep.n_out_len, prep.n_out_ch), dtype=np.float32)
        count = 0
        for f in data_list:
            data_f = np.load(os.path.join(prep.exp_out, db.name, prep.name(), 'data_' + str(f) + '.npy'))
            gt_f = np.load(os.path.join(prep.exp_out, db.name, 'ground_truth', prep.gt_type + '_' + str(f) + '.npy'))

            data_mean = np.mean(data_f, axis=0)
            data_std = np.std(data_f, axis=0)
            data_f -= data_mean
            data_f /= (data_std + 1.e-8)

            data[count:count + data_f.shape[0], :, :] = data_f
            gt[count:count + data_f.shape[0], :, :] = gt_f
            count += data_f.shape[0]
        return data, gt

    def train_and_valid(self, db, prep):
        """Runs training and validation of the model."""
        """
            Arguments:
                prep (Preprocessor): Preprocessor object
        """

        self._define_model(prep)

        output_dir = os.path.join(prep.exp_out, db.name, prep.name(), self.name())
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        output_train_error_path = os.path.join(output_dir, 'errors_train.txt')
        output_valid_error_path = os.path.join(output_dir, 'errors_valid.txt')

        for e in range(0, self.n_epochs + 1):
            print("Epoch:", e)
            if e == 50:
                self.lr = self.lr / 10
            time_start = time.time()

            select_train = np.random.choice(len(prep.train_list), 3, replace=False)
            train_data, train_gt = self._load_data(db, prep, select_train)
            print("select train:", select_train)
            t_shuffler = np.arange(0, train_data.shape[0])
            np.random.shuffle(t_shuffler)

            train_data = train_data[t_shuffler, :, :]
            train_gt = train_gt[t_shuffler, :, :]

            select_valid = np.random.choice(len(prep.valid_list), 3, replace=False)
            print("select valid:", len(prep.train_list) + select_valid)
            valid_data, valid_gt = self._load_data(db, prep, len(prep.train_list) + select_valid)

            print("Saving model...")
            self.save_model(output_dir, e)

            print("Computing training error...")
            t_e_mse = self._valid_model(train_data, train_gt, e)
            print("Computing validation error...")
            v_e_mse = self._valid_model(valid_data, valid_gt, e)

            print("Writing error...")
            with open(output_train_error_path, 'a+') as f_err:
                f_err.write('%s \n' % (np.ravel(t_e_mse)))
            with open(output_valid_error_path, 'a+') as f_err:
                f_err.write('%s \n' % (np.ravel(v_e_mse)))

            if e % 50 == 0:
                self.test(db, prep, e)

            print("Training model...")
            time_start_train = time.time()
            self._train_model(train_data, train_gt)
            time_end_train = time.time()
            print("Time train elapsed:", time_end_train - time_start_train)

            time_end = time.time()
            print("time elapsed:", e, time_end - time_start)

    def test(self, db, prep, model_no):
        """Run testing of the model."""
        """
            Arguments:
                db (Database): Database object
                prep (Preprocessor): Preprocessor object
                model_no (int): ordinal number of model
        """
        test_output_dir = os.path.join(prep.exp_out, db.name, prep.name(), self.name(), 'test_' + str(model_no))
        if not os.path.exists(test_output_dir):
            os.makedirs(test_output_dir)

        for t in prep.test_list:
            test_data, test_gt = self._load_data(db, prep, [t])
            estim = self._test_model(test_data, test_gt)
            err = np.mean(np.sum(np.sum((estim - test_gt) ** 2, axis=1), axis=1))
            print("mean err:", err)
            estim_out_path = os.path.join(test_output_dir, db.subjects_list[t])
            if not os.path.exists(estim_out_path):
                os.makedirs(estim_out_path)
            np.save(os.path.join(estim_out_path, 'estim.npy'), estim)

    def name(self):
        """Method that returns object's name including parameters."""
        return ("%s(n_epochs=%s, w_std=%s)"
                % (type(self).__name__, self.n_epochs, self.w_std))
