"""Class for dMRI descriptor (micro-structure parameters, fODF) estimation using spherical CNN."""

import sys
import os

import numpy as np
import numpy.matlib

import time

import tensorflow as tf
from .. import Estimator

from .nn_utils import weight_variable

from .spherical_cnn_utils import get_s2_fm_points
from .spherical_cnn_utils import sph_to_s2_basis
from .spherical_cnn_utils import s2_to_sph_transposed_conv
from .spherical_cnn_utils import s2_to_sph_basis

S2_P_FOR_S2 = {12: 92, 10: 67, 8: 46, 6: 28, 4: 15, 2: 7, 0: 1}


class EstimatorSphericalUNetS2(Estimator):
    """Class for dMRI descriptor fODF estimation using spherical CNN."""
    """
        Attributes:
            n_epochs (int): number of training and validation epochs
            batch_size (int): number of samples per batch
            lr (float): learning rate
            w_std (float): standard deviation of weights for initialization
            activ_func (tf function): activation function (non-linearity)

            ch_o (list[int]): number of channels at the output of each layer

        Methods:
            set_exp_out: setting experiment output path
            train_and_valid: runs training and validation of the model
            test: runs testing of the model
            save_model: method for model saving
            restore_model: method for model restoring
            name: returns object's name including parameters
    """

    def __init__(self, n_epochs=100, batch_size=256, lr=1.e-3, w_std=0.1, activ_func=tf.nn.relu,
                 ch_out_1=64, sph_ord_c=[8, 4, 2, 0], sph_ord_e=[0, 2, 4, 6, 8, 8],
                 **kwargs):
        super(EstimatorSphericalUNetS2, self).__init__(**kwargs)

        # General attributes for neural networks
        self.n_epochs = n_epochs
        self.batch_size = batch_size
        self.lr = lr
        self.w_std = w_std
        self.activ_func = activ_func

        # Attributes related to the S2 kernels and signals
        self.ch_out_1 = ch_out_1
        self.sph_ord_c = sph_ord_c
        self.sph_ord_e = sph_ord_e

        self.sess = tf.Session()

    def _define_model(self, prep):

        # 1. Allocating memory for input spherical harmonics, ground truth and learning rate
        self.y_sph = tf.placeholder(tf.float32, shape=[None, prep.n_in_len, prep.n_in_ch])
        self.gt = tf.placeholder(tf.float32, shape=[None, prep.n_out_len, prep.n_out_ch])
        self.lr_tf = tf.placeholder(tf.float32)

        # 2. Lists for coordinates, weights, biases, feature maps in space and spectral domain
        coords, weights, biases = [], [], []
        a_signals, a_sph = [], [self.y_sph]

        # 3. Definition of contracting part of the U-net
        with tf.variable_scope('contracting'):

            self.sph_ord_c = [prep.sph_ord] + self.sph_ord_c + [self.sph_ord_c[-1]]
            ch_in, ch_out = prep.n_in_ch, self.ch_out_1

            for i in range(1, len(self.sph_ord_c)):
                # 3.1 Defining contracting layer kernels (coordinates, weights and biases of axially symmetric kernels)
                with tf.variable_scope('layer_' + str(i)):
                    weights.append(weight_variable("w_" + str(i),
                                                   shape=[self.sph_ord_c[i-1] // 2 + 1, ch_in, ch_out],
                                                   std=self.w_std))
                    biases.append(weight_variable("b_" + str(i), shape=[ch_out], std=self.w_std))

                # 3.2 Defining convolutional part and normalization
                o_s, o_sph = self._s2_convolve(a_sph[-1], weights[-1], biases[-1], i - 1)

                a_signals.append(o_s)
                a_sph.append(o_sph)

                ch_in = ch_out
                ch_out = 2 * ch_out

        # 4. Definition of expanding part of the U-net
        with tf.variable_scope('expanding'):
            back = -2
            n_e = len(self.sph_ord_e)

            for i in range(1, n_e):

                ch_in = a_signals[-1].shape[2].value
                ch_out = ch_in // 2
                if i == (n_e - 1):
                    ch_out = 1

                # 4.1 Defining expanding layer kernels (coordinates, weights and biases of axially symmetric kernels)
                with tf.variable_scope('layer_-' + str(n_e - i)):
                    weights.append(weight_variable("w_-" + str(n_e - i),
                                                   shape=[self.sph_ord_e[i] // 2 + 1, ch_in, ch_out],
                                                   std=self.w_std))
                    biases.append(weight_variable("b_-" + str(n_e - i), shape=[ch_out], std=self.w_std))

                # 4.2 Defining transposed convolutional part and normalization
                o_s, o_sph = self._s2_transposed_convolve(a_signals[-1], weights[-1], biases[-1], i, ch_out)

                if len(a_signals) > -back:
                    a_signals.append(tf.concat([a_signals[back], o_s], axis=2))
                else:
                    a_signals.append(o_s)
                a_sph.append(o_sph)
                back -= 2

        # 5. Computing errors, minimization of loss
        self.fod_estim = a_sph[-1]
        self.estims_mse = tf.squeeze(tf.reduce_sum(tf.pow(self.gt - self.fod_estim, 2.), axis=1), axis=1)
        self.mse = tf.reduce_mean(self.estims_mse)
        self.min_loss = tf.train.AdamOptimizer(self.lr_tf).minimize(self.mse)

        self.sess.run(tf.global_variables_initializer())
        self.saver = tf.train.Saver(tf.trainable_variables(), max_to_keep=100000)

    def _s2_convolve(self, y_sph, weights, bias, l):

        # 1. Performing correlation between kernels and input signals in Fourier domain for each order
        s = 0
        for i_idx, i in enumerate(range(0, self.sph_ord_c[l] + 1, 2)):
            o = 2 * i + 1
            # 1.1. Extract spherical harmonics that correspond to the order i
            y_sph_s = tf.gather(y_sph, tf.range(s, s + o), axis=1)
            k_sph_s = weights[i_idx, :, :]

            # 1.2. Perform correlation for the given order
            corr = tf.einsum('nli,io->nlo', y_sph_s, k_sph_s)

            # 1.3 Concatenate correlations
            correlations = corr if not s else tf.concat([correlations, corr], axis=1)
            s += o

        # 2. Get S2 output feature map point coordinates
        s_coord = get_s2_fm_points(self.estim_source_dir, sph_ord=self.sph_ord_c[l])

        # 3. Compute basis for feature map (signal) conversion from spherical harmonics to S2 domain
        s_sph2s2 = tf.convert_to_tensor(sph_to_s2_basis(s_coord, self.sph_ord_c[l], sph_type='real'))

        # 4. Compute output feature maps, S2 domain
        a_s = self.activ_func(tf.einsum('pl,nlc->npc', s_sph2s2, correlations) + bias)

        # 5. Compute basis for feature map conversion from S2 to spherical harmonics domain
        s_s2tosph = tf.convert_to_tensor(s2_to_sph_basis(s_coord, self.sph_ord_c[l + 1], sph_type='real',
                                                         normalize=True, sph_inv='gram_schmidt'))

        # 6. Compute output feature maps, spherical harmonics domain
        a_sph = tf.einsum('npc,lp->nlc', a_s, s_s2tosph)

        return a_s, a_sph

    def _s2_transposed_convolve(self, y_s2, weights, bias, l, ch_out):

        # 1. Upsampling with transposed convolution!!!
        s_s2tosph = tf.convert_to_tensor(s2_to_sph_transposed_conv(self.estim_source_dir,
                                                                   self.sph_ord_e[l - 1],
                                                                   self.sph_ord_e[l],
                                                                   sph_type='real',
                                                                   sph_inv='gram_schmidt'))

        y_sph = tf.einsum('npc,lp->nlc', y_s2, s_s2tosph)

        # 2. Performing correlation between kernels and input signals in Fourier domain for each order
        s = 0
        for i_idx, i in enumerate(range(0, self.sph_ord_e[l] + 1, 2)):
            o = 2 * i + 1
            # 2.1. Extract spherical harmonics that correspond to the order i
            y_sph_s = tf.gather(y_sph, tf.range(s, s + o), axis=1)
            k_sph_s = weights[i_idx, :, :]

            # 2.2. Perform correlation for the given order
            corr = tf.einsum('nli,io->nlo', y_sph_s, k_sph_s)

            # 2.3 Concatenate correlations
            correlations = corr if not s else tf.concat([correlations, corr], axis=1)
            s += o

        # 3. Get S2 output feature map point coordinates
        s_coord = get_s2_fm_points(self.estim_source_dir, sph_ord=self.sph_ord_e[l])

        # 4. Compute basis for feature map (signal) conversion from spherical harmonics to S2 domain
        s_sph2s2 = tf.convert_to_tensor(sph_to_s2_basis(s_coord, self.sph_ord_e[l], sph_type='real'))

        # 5. Compute output feature maps, S2 domain
        if ch_out == 1:
            a_s = tf.einsum('pl,nlc->npc', s_sph2s2, correlations) + bias
        else:
            a_s = self.activ_func(tf.einsum('pl,nlc->npc', s_sph2s2, correlations) + bias)

        # 6. Compute basis for feature map conversion from S2 to spherical harmonics domain
        s_s2tosph = tf.convert_to_tensor(s2_to_sph_basis(s_coord, self.sph_ord_e[l], sph_type='real',
                                                         normalize=True, sph_inv='gram_schmidt'))

        # 7. Compute output feature maps, spherical harmonics domain
        a_sph = tf.einsum('npc,lp->nlc', a_s, s_s2tosph)

        return a_s, a_sph
    
    def _train_step(self, data_batch, gt_batch):
        self.sess.run(self.min_loss, feed_dict={self.y_sph: data_batch, self.gt: gt_batch,
                                                self.lr_tf: self.lr})

    def _train_model(self, train_data, train_gt):
        count = 0
        n_train = train_data.shape[0]
        while count < n_train:
            data_batch = train_data[count:count + self.batch_size, :, :]
            gt_batch = train_gt[count:count + self.batch_size, :, :]
            count += self.batch_size
            self._train_step(data_batch, gt_batch)

    def _valid_step_mse(self, data_batch, gt_batch):
        return self.sess.run(self.estims_mse, feed_dict={self.y_sph: data_batch, self.gt: gt_batch})

    def _valid_estims(self, data_batch):
        return self.sess.run(self.fod_estim, feed_dict={self.y_sph: data_batch})

    def _valid_model(self, data, gt, it):

        err_mse = np.zeros(gt.shape[0])
        n_data = data.shape[0]
        count = 0
        while count < n_data:
            data_batch = data[count:count + self.batch_size, :, :]
            gt_batch = gt[count:count + self.batch_size, :, :]
            err_mse[count:count + self.batch_size] = self._valid_step_mse(data_batch, gt_batch)
            count += self.batch_size
        return np.mean(err_mse)

    def _test_model(self, data, gt):

        estims = np.zeros_like(gt)
        n_data = data.shape[0]
        count = 0
        while count < n_data:
            data_batch = data[count:count + self.batch_size, :, :]
            estims[count:count + self.batch_size, :, :] = self._valid_estims(data_batch)
            count += self.batch_size
        return estims

    def _load_data(self, db, prep, data_list):

        count = 0
        for f in data_list:
            n_vox_path = os.path.join(prep.exp_out, db.name, prep.name(), 'n_vox_' + str(f))
            with open(n_vox_path, 'r') as f_vox:
                count += int(f_vox.readline())

        data = np.zeros((count, prep.n_in_len, prep.n_in_ch), dtype=np.float32)
        gt = np.zeros((count, prep.n_out_len, prep.n_out_ch), dtype=np.float32)
        count = 0
        for f in data_list:
            data_f = np.load(os.path.join(prep.exp_out, db.name, prep.name(), 'data_' + str(f) + '.npy'))
            gt_f = np.load(os.path.join(prep.exp_out, db.name, 'ground_truth', prep.gt_type + '_' + str(f) + '.npy'))

            data[count:count + data_f.shape[0], :, :] = data_f
            gt[count:count + data_f.shape[0], :, :] = gt_f
            count += data_f.shape[0]

        return data, gt

    def train_and_valid(self, db, prep):
        """Runs training and validation of the model."""
        """
            Arguments:
                prep (Preprocessor): Preprocessor object
        """
        np.random.seed(123)

        self._define_model(prep)

        output_dir = os.path.join(prep.exp_out, db.name, prep.name(), self.name())
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        output_train_error_path = os.path.join(output_dir, 'errors_train.txt')
        output_valid_error_path = os.path.join(output_dir, 'errors_valid.txt')

        for e in range(0, self.n_epochs + 1):
            print("Epoch:", e)
            if e == 50:
                self.lr = self.lr / 10
            time_start = time.time()

            select_train = np.random.choice(len(prep.train_list), 3, replace=False)
            train_data, train_gt = self._load_data(db, prep, select_train)
            print("select train:", select_train)
            t_shuffler = np.arange(0, train_data.shape[0])
            np.random.shuffle(t_shuffler)

            train_data = train_data[t_shuffler, :, :]
            train_gt = train_gt[t_shuffler, :, :]

            select_valid = np.random.choice(len(prep.valid_list), 3, replace=False)
            print("select valid:", len(prep.train_list) + select_valid)
            valid_data, valid_gt = self._load_data(db, prep, len(prep.train_list) + select_valid)

            print("Saving model...")
            self.save_model(output_dir, e)

            print("Computing training error...")
            t_e_mse = self._valid_model(train_data, train_gt, e)
            print("Computing validation error...")
            v_e_mse = self._valid_model(valid_data, valid_gt, e)

            print("Writing error...")
            with open(output_train_error_path, 'a+') as f_err:
                f_err.write('%s \n' % (np.ravel(t_e_mse)))
            with open(output_valid_error_path, 'a+') as f_err:
                f_err.write('%s \n' % (np.ravel(v_e_mse)))

            if e % 50 == 0 and e:
                self.test(db, prep, e)

            print("Training model...")
            time_start_train = time.time()

            self._train_model(train_data, train_gt)
            time_end_train = time.time()
            print("Time train elapsed:", time_end_train - time_start_train)

            time_end = time.time()
            print("time elapsed:", e, time_end - time_start)

    def test(self, db, prep, model_no):
        """Run testing of the model."""
        """
            Arguments:
                db (Database): Database object
                prep (Preprocessor): Preprocessor object
                model_no (int): ordinal number of model
        """

        test_output_dir = os.path.join(prep.exp_out, db.name, prep.name(), self.name(), 'test_' + str(model_no))
        if not os.path.exists(test_output_dir):
            os.makedirs(test_output_dir)

        for t in prep.test_list:
            test_data, test_gt = self._load_data(db, prep, [t])
            estim = self._test_model(test_data, test_gt)
            err = np.mean(np.sum(np.sum((estim - test_gt) ** 2, axis=1), axis=1))
            print("mean err:", err)
            estim_out_path = os.path.join(test_output_dir, db.subjects_list[t])
            if not os.path.exists(estim_out_path):
                os.makedirs(estim_out_path)
            np.save(os.path.join(estim_out_path, 'estim.npy'), estim)

    def name(self):
        """Method that returns object's name including parameters."""
        return ("%s(n_epochs=%s, lr=%s, ch_out_1=%s, sph_ord_c=%s, sph_ord_e=%s)"
                % (type(self).__name__, self.n_epochs, self.lr,
                   self.ch_out_1, self.sph_ord_c, self.sph_ord_e))
