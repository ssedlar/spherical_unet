import tensorflow as tf
import numpy as np


def weight_variable(name, shape, std=0.1):
    initial = tf.Variable(tf.truncated_normal(name=name, shape=shape,
                                              stddev=std))
    return initial


def weight_variable_xavier(name, shape, std=0.1):
    initial = tf.get_variable(name, shape,
                              initializer=tf.contrib.layers.
                              xavier_initializer())
    return initial


def orthogonal_initializer(scale = 1.1):
    ''' From Lasagne and Keras.
        Reference: Saxe et al., http://arxiv.org/abs/1312.6120
    '''
    print('Warning -- '
          'You have opted to use the orthogonal_initializer function')
    def _initializer(shape, dtype=tf.float32):
      flat_shape = (shape[0], np.prod(shape[1:]))
      a = np.random.normal(0.0, 1.0, flat_shape)
      u, _, v = np.linalg.svd(a, full_matrices=False)
      # pick the one with the correct shape
      q = u if u.shape == flat_shape else v
      q = q.reshape(shape) #this needs to be corrected to float32
      print('you have initialized one orthogonal matrix.')
      return tf.constant(scale * q[:shape[0], :shape[1]], dtype=tf.float32)
    return _initializer


def weights_orthogonal(name, shape, scale=1.):

    flat_shape = (shape[0], np.prod(shape[1:]))
    a = np.random.normal(0.0, 1.0, flat_shape)
    u, _, v = np.linalg.svd(a, full_matrices=False)
    # pick the one with the correct shape
    q = u if u.shape == flat_shape else v
    q = q.reshape(shape) #this needs to be corrected to float32
    print('you have initialized one orthogonal matrix.')
    variable_np = np.asarray(scale * q[:shape[0], :shape[1]],
                             dtype=np.float32)
    initial = tf.Variable(variable_np, name=name)
    return initial
