"""Utils for spherical and SO(3) signals (bases, sampling points etc.)."""
import os
import numpy as np
from collections import OrderedDict

from dipy.core.geometry import cart2sphere

from scipy.special import sph_harm
from numpy.linalg import pinv
from numpy.linalg import inv


S2_P_FOR_S2 = {12: 92, 10: 67, 8: 46, 6: 28, 4: 15, 2: 7, 0: 1}

import matplotlib.pyplot as plt


def _sph_2_s2_basis(s2_coord, sph_order, sph_type, normalize=True):
    """Computes spherical harmonic basis."""
    """
        Arguments:
            s2_coord (numpy array): S2 points coordinates
            sph_order (int): spherical harmonic order
            sph_type (str): spherical harmonic type; options: 'real'/'complex'
            normalize (str): flag indicating whether to normalize bases or not
        Returns:
            numpy array: spherical harmonic bases (each row is a basis)
    """
    if sph_type == 'real':
        data_type = np.float32
    elif sph_type == 'complex':
        data_type = np.complex64

    n_sph = np.sum([2 * i + 1 for i in range(0, sph_order + 1, 2)])
    sph2sig = np.zeros((s2_coord.shape[0], n_sph), dtype=data_type)
    n_sph = 0
    for i in range(0, sph_order + 1, 2):
        ns = np.zeros(2 * i + 1) + i
        ms = np.arange(-i, i + 1)
        Y_n_m = sph_harm(ms, ns, s2_coord[:, 1:2], s2_coord[:, 0:1])
        if sph_type == 'real' and i > 0:
            Y_n_m[:, 0:i] = np.sqrt(2) * \
                            np.power(-1, np.arange(0, i)) * \
                            np.imag(Y_n_m[:, :i:-1])
            Y_n_m[:, (i + 1):] = np.sqrt(2) * \
                                 np.power(-1, np.arange(i + 1, 2 * i + 1)) * \
                                 np.real(Y_n_m[:, (i + 1):])
        sph2sig[:, n_sph:n_sph + 2 * i + 1] = Y_n_m
        n_sph += 2 * i + 1

    if normalize:
        n = np.dot(sph2sig[:, 0:1].T, sph2sig[:, 0:1])[0, 0]
        sph2sig /= np.sqrt(n)

    return sph2sig


def _sph_2_s2_basis_ax_symm(s2_coord, sph_ord, sph_type, normalize=True):
    """Computes spherical harmonic basis."""
    """
        Arguments:
            s2_coord (numpy array): S2 points coordinates
            sph_ord (int): spherical harmonic order
            sph_type (str): spherical harmonic type; options: 'real'/'complex'
            normalize (str): flag indicating whether to normalize bases or not
        Returns:
            numpy array: spherical harmonic bases (each row is a basis)
    """
    if sph_type == 'real':
        data_type = np.float32
    elif sph_type == 'complex':
        data_type = np.complex64

    n_ax_symm_harmonics = sph_ord // 2 + 1
    sph2sig = np.zeros((s2_coord.shape[0], n_ax_symm_harmonics), dtype=data_type)

    for i_idx, i in enumerate(range(0, sph_ord + 1, 2)):
        ns = np.zeros(1) + i
        ms = np.arange(0, 1)
        sph2sig[:, i_idx:i_idx + 1] = sph_harm(ms, ns, s2_coord[:, 1:2], s2_coord[:, 0:1])

    if normalize:
        n = (2 * sph_ord + 1) * (sph2sig[0, 0] ** 2)
        sph2sig /= np.sqrt(n)

    return sph2sig


def _gram_schmidt_inv(H, n_iter=1000):
    """Performs inversion of spherical harmonic bases with Gram-Schmidt
        orthogonalization."""
    """
        References:
        [1] Yeo, Boon Thye Thomas.
        "Computing spherical transform and convolution on the 2-sphere." 
        Manuscript, MIT (2005).
        
        Arguments:
            H (numpy array): matrix containing spherical harmonics
                (in each column)
            
        Returns:
            inverted spherical harmonic bases
    """
    h_order = (int(np.sqrt(1 + 8 * H.shape[1])) - 3) // 2

    np.random.seed(12345)
    H_inv_final = np.zeros_like(H.T)
    for k in range(n_iter):
        order = []

        count_h = 0
        for i in range(0, h_order + 1, 2):
            order_h = count_h + np.arange(0, 2 * i + 1)
            np.random.shuffle(order_h)
            order.extend(list(order_h))
            count_h += 2 * i + 1

        deorder = np.argsort(order)

        H_inv = np.zeros_like(H.T)
        for i in range(H_inv.shape[0]):
            H_inv[i, :] = H[:, order[i]]
            for j in range(0, i):
                if np.sum(H_inv[j, :] ** 2) > 1.e-8:
                    H_inv[i, :] -= np.sum(H_inv[i, :] * H_inv[j, :]) / \
                                   (np.sum(H_inv[j, :] ** 2) + 1.e-8) * \
                                   H_inv[j, :]
            if np.sqrt(np.sum(H_inv[i, :] ** 2)) > 1.e-4:
                H_inv[i, :] /= np.sqrt(np.sum(H_inv[i, :] ** 2))

        H_inv_final += H_inv[deorder, :]
    H_inv_final /= n_iter

    return H_inv_final


def _pinv(B):
    return pinv(B)


def s2_to_sph_transposed_conv(estim_source_dir, sph_ord_in, sph_ord_out, sph_type, normalize=True,
                              sph_inv='gram_schmidt'):

    s2_coord_out = get_s2_fm_points(estim_source_dir, sph_ord=sph_ord_out)

    sph2sig = _sph_2_s2_basis(s2_coord_out, sph_ord_out, sph_type, normalize)
    if sph_inv == 'pinv':
        sph_basis_out_inv = _pinv(sph2sig)
    elif sph_inv == 'gram_schmidt':
        sph_basis_out_inv = _gram_schmidt_inv(sph2sig)
    else:
        raise ValueError("Invalid sph inversion.")

    n_points = S2_P_FOR_S2[sph_ord_in]
    sph_basis_inv_red = sph_basis_out_inv[:, 0:n_points]

    return sph_basis_inv_red


def s2_to_sph_basis(s2_coords, sph_order, sph_type, normalize=True,
                    sph_inv='gram_schmidt'):
    """Computes inverse spherical harmonic basis for conversion from signal
        to spherical harmonic domain."""
    """
        Arguments:
            s2_coords (numpy array): containing gradient directions
            sph_order (int): maximum spherical harmonic order
            sph_type (str): spherical harmonic type; 
                options: 'real'/ 'complex'
            normalize (bool): bool indicating whether to normalize basis
            sph_inv (str): 
        Returns:
            numpy array: inverse spherical harmonic bases
    """
    sph2sig = _sph_2_s2_basis(s2_coords, sph_order, sph_type, normalize)

    if sph_inv == 'pinv':
        return _pinv(sph2sig)
    elif sph_inv == 'gram_schmidt':
        return _gram_schmidt_inv(sph2sig)
    else:
        raise ValueError("Invalid sph inversion.")


def s2_to_sph_basis_ax_symm(s2_coord, sph_ord, sph_type, normalize=True,
                            sph_inv='gram_schmidt'):
    """Computes basis for inverse spherical harmonic transform (from signal to spherical harmonic domain)"""
    """
        Note: kernel is considered as sum of delta impulses, so inverse is simple conjugate transpose

        Arguments:
            s2_coord (numpy array): S2 points coordinates
            sph_ord (int): maximum spherical harmonic order
            sph_type (str): spherical harmonic type; options: 'real'/ 'complex'
            normalize (bool): bool indicating whether to normalize basis
        Returns:
            numpy array: inverse spherical harmonic bases for kernel transform
    """
    sph2sig = _sph_2_s2_basis_ax_symm(s2_coord, sph_ord, sph_type, normalize)

    return sph2sig.T


def sph_to_s2_basis(s2_coords, sph_order, sph_type, normalize=True):
    """Computes spherical harmonic basis for conversion from signal to
        spherical harmonic domain."""
    """
        Arguments:
            s2_coord_dict (numpy array): gradient directions
            sph_order (int): maximum spherical harmonic order
            sph_type (str): spherical harmonic type; 
                options: 'real'/ 'complex'
            normalize (bool): bool indicating whether to normalize basis
        Returns:
            (numpy array): inverse spherical harmonic basis per shell
    """
    sph2sig = _sph_2_s2_basis(s2_coords, sph_order, sph_type, normalize)
    return sph2sig


def get_s2_kernel_points_ax_symm(max_theta=np.pi / 8):
    """Generating S2 axially symmetric kernel points."""
    """
        Arguments:
            max_theta (float): maximum inclination angle
        Returns:
            coordinates of S2 kernel points
    """
    angles = np.zeros((2, 2))
    angles[0, 0] = max_theta
    return angles


def get_s2_signal_points(prt, coord_sys='sph'):
    """Creating dictionary of shells and corresponding gradient
        coordinates."""
    """
        Arguments:
            prt (Protocol): protocol object
        Returns:
            OrderedDict: dictionary containing S2 gradient coordinates 
            for each shell
    """
    bvecs = OrderedDict()
    for k in prt.shell_masks:
        bvecs[k] = np.zeros((prt.shell_masks[k].shape[0], 3))
        try:
            bvecs_cart = prt.gradient_scheme.bvecs[prt.shell_masks[k], :]
        except:
            bvecs_cart = \
                prt.gradient_scheme.gradient_directions[prt.shell_masks[k], :]

        for i in range(prt.shell_masks[k].shape[0]):
            if coord_sys == 'sph':
                bvecs[k][i, :] = cart2sphere(bvecs_cart[i, 0],
                                             bvecs_cart[i, 1],
                                             bvecs_cart[i, 2])
                if bvecs[k][i, 2] < 0:
                    bvecs[k][i, 2] = 2 * np.pi + bvecs[k][i, 2]
            else:
                bvecs[k][i, :] = bvecs_cart[i, :]
    return bvecs


def get_s2_fm_points(estim_dir_path, sph_ord=12, n_uni_points=None):
    """Creates an array of S2 point coordinates for feature maps."""
    """
        Description:
            Creates an array of S2 feature maps' points. 
            Azimuth and tilt angles are uniformly distributed on
            a sphere according to the Q-space-sampling scheme [1].
        References:
        [1] Caruyer, Emmanuel, et al. 
            "Design of multishell sampling schemes with uniform 
            coverage in diffusion MRI."
            Magnetic resonance in medicine 69.6 (2013): 1534-1540.
            url: http://www.emmanuelcaruyer.com/q-space-sampling.php
        Arguments:
            estim_dir_path (str): path to the estimators' source files 
                directory
            sh_order (int): maximal spherical harmonic order
            n_uni_points (int): number of uniformly distributed points
        Returns:
            numpy array: coordinates of S2 points
    """
    if n_uni_points is None:
        n_uni_points = S2_P_FOR_S2[sph_ord]
    sampling_file_path = os.path.join(estim_dir_path,
                                      'sampling_points',
                                      'samples_' + str(n_uni_points) +
                                      '.txt')
    with open(sampling_file_path) as f:
        lines = f.readlines()
    s2_points = []
    for l in lines:
        if l.startswith('1'):
            p = [float(s) for s in str.split(l.strip())[1:]]
            p_sph = cart2sphere(p[0], p[1], p[2])
            s2_points.append([p_sph[1], p_sph[2]])
    return np.array(s2_points)
