"""Class for diffusion MRI real database management;
    child class of Database."""
from .. import Database


class DatabaseReal(Database):
    """Class for diffusion MRI real database management;
        child class of Database."""

    """
        Attributes:

        Methods:
            load_subjects: loads subject names and splits them into
                train, valid and test subsets
            name: returns objects name
    """

    def __init__(self, **kwargs):
        super(DatabaseReal, self).__init__(**kwargs)

    def load_subjects(self):
        """Loads subject names and splits them into train, valid and test
            subsets."""
        raise NotImplementedError()

    @property
    def name(self):
        """Returns object's name."""
        raise NotImplementedError()
