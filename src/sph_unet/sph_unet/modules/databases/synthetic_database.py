"""Class for diffusion MRI synthetic database management; child class of Database."""
from .. import Database
from .. import protocols


class DatabaseSynthetic(Database):
    """Class for diffusion MRI synthetic database management; child class of Database."""

    """
        Attributes:
            protocol_class : acquisition protocol class
            snr (float): signal to noise ratio
            n_samples (int): number of synthetic samples to generate
            prt_class (Protocol class): acquisition protocol
        Methods:
            generate_data: generate training, validation, testing subsets
            set_protocol_class: set acquisition protocol class
            name: returns object's name including parameters
    """

    def __init__(self, snr=None, n_samples=20000, **kwargs):
        super(DatabaseSynthetic, self).__init__(**kwargs)

        self.snr = snr
        self.n_samples = n_samples
        self.prt_class = None
        self.syn_db_path = None

    def generate_data(self):
        """Runs synthetic data generation."""
        raise NotImplementedError()

    def set_protocol_class(self, prt_class):
        """Sets acquisition protocol class."""
        """
            Arguments:
                prt_class(Protocol class): acquisition protocol class
        """
        self.prt_class = prt_class

    def set_synthetic_db_path(self, syn_db_path):
        """Sets synthetic database path."""
        """
            Arguments:
                syn_db_path (str): synthetic database path
        """
        self.syn_db_path = syn_db_path

    def name(self):
        """Returns object's name including parameters."""
        raise NotImplementedError()

