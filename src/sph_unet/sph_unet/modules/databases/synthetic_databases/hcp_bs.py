"""Class for HCP 'ball&stick' synthetic database management; child class of DatabaseSynthetic."""

"""
Reproduction of database used in:
[1] Lin, Zhichao, et al. 
"Fast learning of fiber orientation distribution function for MR tractography using convolutional neural network."
Medical physics 46.7 (2019): 3101-3116.

Synthetic data generation according to:
[2] Wilkins, Bryce, et al.
"Fiber estimation and tractography in diffusion MRI: development of simulated brain images and
comparison of multi-fiber analysis methods at clinical b-values."
Neuroimage 109 (2015): 341-356.

"""

import os
import shutil
from .. import DatabaseSynthetic
from ..real_databases import DatabaseHCP3T
from ...protocols import ProtocolHCP3T

from dipy.reconst.shm import QballModel
from dipy.reconst.odf import gfa

from dipy.data import get_sphere
from dipy.core.geometry import sphere2cart, cart2sphere

from collections import OrderedDict

import nibabel as nib
import numpy as np

from dipy.core.gradients import gradient_table
import time


class DatabaseHCPbs(DatabaseSynthetic, DatabaseHCP3T):
    """Class DatabaseHCPbs for synthetic data based on ball and stick model [1, 2]."""
    """
        Attributes:
            D0_wm: (float)
                free diffusion parameter for white matter
            DO_gm: (float)
                free diffusion parameter for gray matter
            D0_csf: (float)
                free diffusion paramter for cerebrospinal fluid
            Dx: (numpy array [3 x 3])
                single fiber tensor
            fr23th: (float)
                threshold applied on second and third fiber volume fractions to eliminate 
                false fibers due to overfitting
            gfa_tol: (float)
                generalized fractional anisotropy difference tolerance threshold between synthetic and real data
            n_max_iter: (int)
                maximum number of iterations when calculating synthetic signal for one voxel
            SNR: (float)
                signal to noise ration to add to synthetic data
            n_points: (int/str)
                number of points to select from original sampling scheme
            data_name: (str)
                name of the file where real dMRI data is stored
            diff_name: (str)
                name of the folder to store synthetic data

    """
    def __init__(self, D0_wm=0.68, D0_gm=0.96, D0_csf=2.25, Dx=[1.7, 0.17, 0.17], fr23th=0.1,
                 gfa_tol=0.00005, n_max_iter=1000, SNR=18, n_points='all', data_name='data',
                 diff_name='Diffusion_synthetic', **kwargs):
        super(DatabaseHCPbs, self).__init__(**kwargs)

        self.gfa_tol = gfa_tol
        self.D0_wm = D0_wm * 0.001    # [s/mm^2]
        self.D0_gm = D0_gm * 0.001    # [s/mm^2]
        self.D0_csf = D0_csf * 0.001  # [s/mm^2]
        self.Dx = np.diag([Dx[i] * 0.001 for i in range(3)])  # [s/mm^2]

        self.fr23th = fr23th
        self.gfa_tol = gfa_tol
        self.n_max_iter = n_max_iter
        self.header = None
        self.affine = None
        self.SNR = SNR
        self.n_points = n_points
        self.data_name = data_name
        self.diff_name = diff_name

    def _get_path_templates(self):

        # Defining template paths
        self.bedpostx_template_path = os.path.join(self.db_path, '{}', 'T1w', 'Diffusion.bedpostX')
        self.orig_template_path = os.path.join(self.db_path, '{}', 'T1w', 'Diffusion')
        self.ttgen5_path = os.path.join(self.db_path, '{}', 'T1w', 'Diffusion', '5ttgen.nii.gz')
        self.synthetic_data_path = os.path.join(self.db_path, '{}', 'T1w', self.diff_name)

    def _load_data(self, subj_id):

        S0_path = os.path.join(self.bedpostx_template_path.format(subj_id), 'mean_S0samples.nii.gz')
        data_path = os.path.join(self.orig_template_path.format(subj_id), 'data.nii.gz')
        tissues_path = os.path.join(self.ttgen5_path.format(subj_id))

        S0 = nib.load(S0_path).get_data()

        data_nii = nib.load(data_path)
        self.header = data_nii.header.copy()
        self.affine = data_nii.affine.copy()

        data = data_nii.get_data()
        tissues = nib.load(tissues_path).get_data()[::-1, :, :, :]

        brain_mask = np.asarray(np.sum(tissues, axis=3) > 0, dtype='uint8')

        return S0, brain_mask, data, tissues

    def _load_fiber_data(self, s, wm_mask):

        # theta (inclination angle) [0, pi]
        # phi (azimuth angle) [-pi, pi]

        fb_fr, fb_th, fb_ph = OrderedDict(), OrderedDict(), OrderedDict()
        for f_idx in range(1, 4):
            f_path = os.path.join(self.bedpostx_template_path.format(s), 'mean_f' + str(f_idx) + 'samples.nii.gz')
            dyads_path = os.path.join(self.bedpostx_template_path.format(s), 'dyads' + str(f_idx) + '.nii.gz')

            dyads = nib.load(dyads_path).get_data()[wm_mask, :]
            sph_coord = cart2sphere(dyads[:, 0], dyads[:, 1], dyads[:, 2])

            fb_fr[f_idx] = nib.load(f_path).get_data()[wm_mask]
            fb_th[f_idx] = sph_coord[1]
            fb_ph[f_idx] = sph_coord[2]

            if f_idx in [2, 3]:
                fb_fr[f_idx] *= fb_fr[f_idx] > self.fr23th

        return fb_fr, fb_th, fb_ph

    def _get_tissue_masks(self, tissues, brain_mask):

        tissue_masks = np.argmax(tissues, axis=3)

        wm_mask = (tissue_masks == 2)

        gm_mask_1 = (tissue_masks == 0) * (brain_mask == 1)
        gm_mask_2 = (tissue_masks == 1)
        gm_mask = gm_mask_1 + gm_mask_2

        csf_mask = (tissue_masks == 3)

        return wm_mask, gm_mask, csf_mask

    def _compute_diffusion_tensors(self, fb_th, fb_ph):

        Dk = OrderedDict()
        for f_idx in range(1, 4):
            thetas, phis = fb_th[f_idx], fb_ph[f_idx]
            vs = np.asarray(sphere2cart(np.ones_like(thetas), thetas, phis))
            x_and_v = np.copy(vs)
            x_and_v[0, :] += 1.
            xv = vs[0, :] + 1

            Rx = np.einsum('in,jn->ijn', x_and_v, x_and_v) / (xv + 1.e-8)

            Rx[0, 0, :] -= 1.
            Rx[1, 1, :] -= 1.
            Rx[2, 2, :] -= 1.

            Dk[f_idx] = np.einsum('ijn,kjn->ikn', np.einsum('ijn,jk->ikn', Rx, self.Dx), Rx)

        return Dk

    def _compute_diffusion_tensors_jitter_response(self, fb_th, fb_ph):

        Dx = np.zeros((3, 3, fb_th[1].shape[0]))
        Dx[0, 0, :] = self.Dx[0, 0] + self.Dx[0, 0] * np.random.uniform(-0.1, 0.1, fb_th[1].shape[0])
        dx12 = self.Dx[1, 1] + self.Dx[1, 1] * np.random.uniform(-0.1, 0.1, fb_th[1].shape[0])
        Dx[1, 1, :] = dx12
        Dx[2, 2, :] = dx12

        Dk = OrderedDict()
        for f_idx in range(1, 4):
            thetas, phis = fb_th[f_idx], fb_ph[f_idx]
            vs = np.asarray(sphere2cart(np.ones_like(thetas), thetas, phis))

            x_and_v = np.copy(vs)
            x_and_v[0, :] += 1.
            xv = vs[0, :] + 1

            Rx = np.einsum('in,jn->ijn', x_and_v, x_and_v) / (xv + 1.e-8)

            Rx[0, 0, :] -= 1.
            Rx[1, 1, :] -= 1.
            Rx[2, 2, :] -= 1.

            Dk[f_idx] = np.einsum('ijn,kjn->ikn', np.einsum('ijn,jkn->ikn', Rx, Dx), Rx)

        return Dk

    def _gfa_invivo(self, protocol, data, brain_mask, sphere):

        gfa_invivo_v = OrderedDict()
        shells = list(protocol.shell_masks.keys())
        data_bm = data[brain_mask, :]

        for sh in shells:

            gfa_invivo_v[sh] = np.zeros_like(brain_mask, dtype=np.float32)

            shell_mask = sorted(list(np.ravel(np.where(protocol.gradient_scheme.b0s_mask))) +\
                                list(protocol.shell_masks[sh]))

            b_vals = protocol.gradient_scheme.bvals[shell_mask]
            b_vecs = protocol.gradient_scheme.bvecs[shell_mask, :]

            d, D = np.ones_like(b_vals) * protocol.d, np.ones_like(b_vals) * protocol.D
            gtabs_per_sh = gradient_table(b_vals, b_vecs, D, d, protocol.b0_th / 1.e6)

            data_m_sh = data_bm[:, shell_mask]

            qb_model = QballModel(gtabs_per_sh, sh_order=12)
            qb_fit = qb_model.fit(data_m_sh)
            qb_odf = qb_fit.odf(sphere)
            gfa_invivo_v[sh][brain_mask] = gfa(qb_odf)

        return gfa_invivo_v

    @staticmethod
    def load_protocol(protocol_path):
        """Loads corresponding acquisition protocol for a subject."""
        """
            Arguments:
                protocol_path (str): path to the protocol of one subject
            Returns:
                Protocol: object containing acquisition protocol
        """
        prt = ProtocolHCP3T(protocol_path=protocol_path, protocol_lib='dipy')
        prt.load_gradient_scheme()
        return prt

    def get_dmri_template_path(self, template_inter='{}/T1w'):
        """Gets template path to the dMRI data and protocol."""
        """
            Returns:
                str: template path to the dMRI data and protocol
        """
        return self.dmri_template_path

    def set_dmri_template_path(self, template_inter='{}/T1w/'):
        """Sets template path to the dMRI data and protocol."""
        """
            Arguments:
                template_iter (str): relative template path
                    to the dMRI data and protocol with respect
                    to database path
        """
        self.dmri_template_path = os.path.join(self.db_path, template_inter, self.diff_name)

    def generate_data(self, subject_idx):
        """Runs synthetic data generation."""

        # Sphere for ODF estimation
        sphere = get_sphere('repulsion724')
        self._get_path_templates()

        # Dictionaries for storing fractions and fiber orientations

        for s in self.subjects_list[subject_idx:subject_idx+1]:

            if not os.path.exists(self.synthetic_data_path.format(s)):
                os.makedirs(self.synthetic_data_path.format(s))
            else:
                if os.path.exists(os.path.join(self.synthetic_data_path.format(s), 'done')):
                    continue

            # Loading data
            S0, brain_mask, data, tissues = self._load_data(s)

            # Tissue masks
            wm_mask, gm_mask, csf_mask = self._get_tissue_masks(tissues, brain_mask)

            # Get non-diffusion weighted values for each tissue type
            n_wm = np.sum(wm_mask)
            S0_gm, S0_wm, S0_csf = S0[gm_mask], S0[wm_mask], S0[csf_mask]

            # Determining standard deviation of Rician noise from homogeneous white matter regions
            S0_wm_m = np.mean(S0[tissues[:, :, :, 2] == 1.])
            noise_std = S0_wm_m / self.SNR

            # Loading fractions and fiber orientations; fiber filtering
            fb_fr, fb_th, fb_ph = self._load_fiber_data(s, wm_mask)

            # Normalizing fiber fractions so that they sum to 1
            fb_fr['sum'] = fb_fr[1] + fb_fr[2] + fb_fr[3]
            for f_idx in range(1, 4):
                fb_fr[f_idx] /= (fb_fr['sum'] + 1.e-8)

            # Computing diffusion tensors
            if self.diff_name == 'Diffusion_synthetic':
                Dk = self._compute_diffusion_tensors(fb_th, fb_ph)
            elif self.diff_name == 'Diffusion_synthetic_jitter_response':
                Dk = self._compute_diffusion_tensors_jitter_response(fb_th, fb_ph)
            else:
                raise ValueError("Invalid diffusion name")

            # Loading original HCP protocols
            protocol = self.load_protocol(self.orig_template_path.format(s))
            shells = list(protocol.shell_masks.keys())

            # Computing per shell in-vivo GFA
            gfa_invivo_v = self._gfa_invivo(protocol, data, brain_mask == 1, sphere)

            # Allocating memory for synthetic data and synthetic data with noise
            h, w, d, ng = data.shape
            synthetic = np.zeros_like(data, dtype=np.float32)
            synthetic_with_noise = np.zeros_like(data, dtype=np.float32)

            b0_idx = np.where(protocol.gradient_scheme.b0s_mask)[0]

            for bidx in b0_idx:
                synthetic[:, :, :, bidx] = S0

            wm_idx = np.where(wm_mask)

            # Generating synthetic data per shell
            for sh_idx, sh in enumerate(shells):
                time_start_6 = time.time()
                fb_fr_0 = np.zeros_like(wm_mask, dtype=np.float32)

                b_list = protocol.shell_masks[sh]
                b_vals = protocol.gradient_scheme.bvals[b_list]
                b_vecs = protocol.gradient_scheme.bvecs[b_list, :]

                for bidx, b_val in zip(b_list, b_vals):
                    synthetic[gm_mask, bidx:bidx+1] = np.outer(S0_gm, np.exp(-b_val * self.D0_gm))
                    synthetic[csf_mask, bidx:bidx+1] = np.outer(S0_csf, np.exp(-b_val * self.D0_csf))

                fiber_signals = np.zeros((n_wm, len(b_list)), dtype=np.float32)
                for f_idx in range(1, 4):
                    gDg = np.einsum('pjn,pj->pn', np.einsum('pi,ijn->pjn', b_vecs, Dk[f_idx]), b_vecs)
                    se = np.exp(-gDg.transpose([1, 0]) * b_vals).transpose([1, 0])
                    sf = fb_fr[f_idx] * S0_wm
                    fiber_signals += (sf * se).transpose([1, 0])

                gfa_invivo_wm = gfa_invivo_v[sh][wm_mask]

                sg_b_list = sorted(list(b0_idx) + list(b_list))
                sg_b_vals = protocol.gradient_scheme.bvals[sg_b_list]
                sg_b_vecs = protocol.gradient_scheme.bvecs[sg_b_list, :]

                d, D = np.ones_like(sg_b_vals) * protocol.d, np.ones_like(sg_b_vals) * protocol.D
                s_gtabs_per_sh = gradient_table(sg_b_vals, sg_b_vecs, D, d, protocol.b0_th / 1.e6)
                qb_model = QballModel(s_gtabs_per_sh, sh_order=12)

                s_wm_iso = np.outer(S0_wm, np.exp(-b_vals * self.D0_wm))

                s_tot = np.zeros((1, ng), dtype=np.float32)
                count_wrong, count_iter_max = 0, 0
                fb_fr_sum_123 = fb_fr[1] + fb_fr[2] + fb_fr[3]

                for v_idx in range(0, n_wm):
                    if v_idx % 10000 == 0:
                        print("v idx:", v_idx)

                    s_tot[0, b0_idx] = S0_wm[v_idx]
                    f0, f0_step = 0., 0.5
                    iter_no = 0
                    while iter_no < self.n_max_iter:
                        if fb_fr_sum_123[v_idx] == 0:
                            s_tot[0, b_list] = s_wm_iso[v_idx, :]
                            f0 = 1.
                            break
                        else:
                            s_tot[0, b_list] = \
                                (1 - f0) * fiber_signals[v_idx, :] + f0 * s_wm_iso[v_idx, :]

                        qb_fit = qb_model.fit(s_tot[:, sg_b_list])
                        gfa_s = gfa(qb_fit.odf(sphere))

                        gfa_diff = np.abs(gfa_invivo_wm[v_idx] - gfa_s)
                        if gfa_diff < self.gfa_tol:
                            break

                        if f0 == 0 and gfa_s < gfa_invivo_wm[v_idx]:
                            count_wrong += 1
                            break

                        if (iter_no + 1) == self.n_max_iter:
                            count_iter_max += 1
                            print("count iter max:", count_iter_max)
                            break

                        if gfa_s > gfa_invivo_wm[v_idx]:
                            f0 += f0_step
                            if f0 > 1:
                                f0 -= f0_step
                                f0_step /= 2
                                continue
                        if gfa_s < gfa_invivo_wm[v_idx]:
                            f0 -= f0_step
                            if f0 < 0:
                                f0 += f0_step
                                f0_step /= 2
                                continue
                            f0_step /= 2
                            f0 += f0_step

                        iter_no += 1

                    fb_fr_0[wm_idx[0][v_idx], wm_idx[1][v_idx], wm_idx[2][v_idx]] = f0
                    synthetic[wm_idx[0][v_idx], wm_idx[1][v_idx], wm_idx[2][v_idx], b_list] = s_tot[0, b_list]

                fb_fr_0_path = os.path.join(self.synthetic_data_path.format(s),
                                            'fb_fr_0_shell_' + str(sh_idx + 1) + '.npy')
                np.save(fb_fr_0_path, fb_fr_0)

                time_end_6 = time.time()
                print("Shell:", s, sh)
                print("Time elapsed 6:", time_end_6 - time_start_6)
                print("n wrong, n iter max:", count_wrong, count_iter_max)
                print("\n")

            synthetic_data_path = os.path.join(self.synthetic_data_path.format(s), 'data.nii.gz')
            synthetic_data = nib.nifti1.Nifti1Image(synthetic, affine=self.affine, header=self.header)
            nib.save(synthetic_data, synthetic_data_path)

            brain_mask_path = os.path.join(self.synthetic_data_path.format(s), 'brain_mask_5tt.nii.gz')
            nodif_src_path = os.path.join(self.orig_template_path.format(s), 'nodif_brain_mask.nii.gz')
            mask_header = nib.load(nodif_src_path).header.copy()
            mask_affine = nib.load(nodif_src_path).affine.copy()
            brain_mask_data = nib.nifti1.Nifti1Image(brain_mask, affine=mask_affine, header=mask_header)
            nib.save(brain_mask_data, brain_mask_path)

            bm_idx = np.where(brain_mask)
            for b_idx in range(bm_idx[0].shape[0]):
                synthetic_with_noise[bm_idx[0][b_idx], bm_idx[1][b_idx], bm_idx[2][b_idx], :] = \
                np.sqrt((synthetic[bm_idx[0][b_idx], bm_idx[1][b_idx], bm_idx[2][b_idx], :] +
                         (noise_std / np.sqrt(2)) * np.random.randn(ng)) ** 2 +
                        ((noise_std / np.sqrt(2)) * np.random.randn(ng)) ** 2)

            synthetic_data_with_noise_path = os.path.join(self.synthetic_data_path.format(s),
                                                          'data_SNR=' + str(self.SNR) + '.nii.gz')
            synthetic_data_noise = nib.nifti1.Nifti1Image(synthetic_with_noise, None, header=self.header)
            nib.save(synthetic_data_noise, synthetic_data_with_noise_path)

            # bvals and bvecs
            bval_src_path = os.path.join(self.orig_template_path.format(s), 'bvals')
            bval_dst_path = os.path.join(self.synthetic_data_path.format(s), 'bvals')

            bvec_src_path = os.path.join(self.orig_template_path.format(s), 'bvecs')
            bvec_dst_path = os.path.join(self.synthetic_data_path.format(s), 'bvecs')

            shutil.copy(bval_src_path, bval_dst_path)
            shutil.copy(bvec_src_path, bvec_dst_path)

            with open(os.path.join(self.synthetic_data_path.format(s), 'done'), 'w') as f:
                f.close()


    def get_input_path_template(self):
        """Creates subject's template path to raw dMRI data and protocol."""
        """
            Returns:
                str: template path to the dMRI data and protocol
        """
        return os.path.join(self.db_path, '{}', 'T1w', 'Diffusion_synthetic_jitter_response')

    def generate_ds_data(self):

        self._get_path_templates()

        for s_idx, s in enumerate(self.subjects_list):

            input_path = self.synthetic_data_path.format(s)
            protocol = self.get_protocol(input_path)
            output_path = input_path + '(n_points={})'.format(str(self.n_points))

            if not os.path.exists(output_path):
                os.makedirs(output_path)
            else:
                if os.path.exists(os.path.join(output_path, 'done')):
                    continue

            data_nib = nib.load(os.path.join(input_path, 'data_SNR=18.nii.gz'))

            new_header = data_nib.header.copy()
            b0_list = np.where(protocol.gradient_scheme.b0s_mask)[0]
            b_list = np.where(protocol.gradient_scheme.b0s_mask == False)[0]

            b_list_select = sorted(list(b0_list) + list(b_list[0:self.n_points]))
            new_header['dim'][4] = len(b_list)

            data = data_nib.get_data()
            data_new = data[:, :, :, b_list_select]

            ds_data_nib = nib.nifti1.Nifti1Image(data_new, None, header=new_header)
            nib.save(ds_data_nib, os.path.join(output_path, self.data_name + '.nii.gz'))

            input_bvecs_path = os.path.join(input_path, 'bvecs')
            l_ds = []
            with open(input_bvecs_path, 'r') as f:
                lines = f.readlines()
                for l in lines:
                    l_split = str.split(l)
                    l_ds.append([l_split[idx] for idx in b_list_select])

            output_bvecs_path = os.path.join(output_path, 'bvecs')
            with open(output_bvecs_path, 'w') as f:
                for l in l_ds:
                    for b in l:
                        f.write(b + ' ')
                    f.write('\n')

            input_bvals_path = os.path.join(input_path, 'bvals')
            l_ds = []
            with open(input_bvals_path, 'r') as f:
                lines = f.readlines()
                for l in lines:
                    l_split = str.split(l)
                    l_ds.append([l_split[idx] for idx in b_list_select])

            output_bvals_path = os.path.join(output_path, 'bvals')
            with open(output_bvals_path, 'w') as f:
                for l in l_ds:
                    for b in l:
                        f.write(b + ' ')
                    f.write('\n')

            with open(os.path.join(output_path, 'done'), 'w') as f:
                f.close()

    @property
    def name(self):
        """Returns object's name including parameters."""
        return ("%s(n_subjects=%s, split=%s)"
                % (type(self).__name__, self.n_subjects, self.split))

