"""Class for HCP 3T database management; child class of DatabaseReal."""

import os

from .. import DatabaseReal
from ...protocols import ProtocolHCP3T


class DatabaseHCP3T(DatabaseReal):
    """Class for HCP 3T database management; child class of DatabaseReal."""
    """
        Attributes:
            n_subjects (int): number of subjects (or to consider) in the
                database
            split (list[float]): fractions for split of data into
                train, valid and test subsets
            n_train (int): number of subject to use for training
            
            subjects_list (list[str]): list of the subjects in the database
            train_subjects (list[str]): list of the subjects in the training
                subset
            valid_subjects (list[str]): list of the subjects in the validation
                subset
            test_subjects (list[str]): list of the subjects in the testing
                subset
            
            data_name (str): name of the dMRI scan
            dmri_template_path (str): template path to the dMRI data and
                protocol for one subject
            
        Methods:
            get_dmri_template_path: gets template path to the dMRI data
            set_dmri_template_path: sets template path to the dMRI data
            load_protocol: loads database compatible protocol for one subject
            load_subjects: loads list of subjects and splits them into
                train, valid and test subsets
            name: returns object's name
    """
    def __init__(self, n_subjects=50, split=[0.6, 0.2, 0.2],
                 data_name='data', **kwargs):
        super(DatabaseHCP3T, self).__init__(**kwargs)

        self.n_subjects = n_subjects
        self.split = split

        self.subjects_list = None
        self.train_subjects = None
        self.valid_subjects = None
        self.test_subjects = None

        self.data_name = data_name
        self.dmri_template_path = None

    def get_dmri_template_path(self, template_inter='{}/T1w/Diffusion'):
        """Gets template path to the dMRI data and protocol."""
        """
            Returns:
                str: template path to the dMRI data and protocol
        """
        return self.dmri_template_path

    def set_dmri_template_path(self, template_inter='{}/T1w/Diffusion'):
        """Sets template path to the dMRI data and protocol."""
        """
            Arguments:
                template_iter (str): relative template path
                    to the dMRI data and protocol with respect
                    to database path
        """
        self.dmri_template_path = os.path.join(self.db_path, template_inter)

    @staticmethod
    def load_protocol(protocol_path):
        """Loads corresponding acquisition protocol for a subject."""
        """
            Arguments:
                protocol_path (str): path to the protocol of one subject
            Returns:
                Protocol: object containing acquisition protocol
        """
        prt = ProtocolHCP3T(protocol_path=protocol_path, protocol_lib='dipy')
        prt.load_gradient_scheme()
        return prt

    def load_subjects(self):
        """Loads and splits list of subjects into train, valid and test."""
        self.subjects_list =\
            sorted(os.listdir(self.db_path))[0:self.n_subjects]
        if not len(self.subjects_list):
            raise ValueError("Subject list is empty.")

        n_train = int(len(self.subjects_list) * self.split[0])
        self.train_subjects = self.subjects_list[0:n_train]
        n_valid = int(len(self.subjects_list) * self.split[1])
        self.valid_subjects = self.subjects_list[n_train:n_train + n_valid]
        self.test_subjects = self.subjects_list[n_train + n_valid:]

    @property
    def name(self):
        """Returns object's name including parameters."""
        return ("%s(n_subjects=%s, split=%s)"
                % (type(self).__name__, self.n_subjects, self.split))
