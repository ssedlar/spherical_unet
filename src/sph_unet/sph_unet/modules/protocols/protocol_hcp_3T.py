"""Class for HCP 3T dMRI scanning protocol management; child class of Protocol."""

from .. import Protocol
import os
import numpy as np
from dmipy.core.acquisition_scheme import acquisition_scheme_from_bvalues
from dipy.core.gradients import gradient_table
from collections import OrderedDict


class ProtocolHCP3T(Protocol):
    """Class for HCP 3T dMRI scanning protocol management; child class of Protocol."""

    """
        Attributes:
            shell_masks (OrderedDict): indices of dMRI signal per shell
            gradient_scheme (dipy's GradientTable/ dmipy's DmipyAcquisitionScheme object): gradient scheme
            d (float): acquisition pulse duration [s]
            D (float): time distance between acquisition pulses [s]

        Methods:
            load_gradient_scheme: loads acquisition protocol 
            name: returns object's name including parameters
    """
    def __init__(self, b_shell_abs_d=50*1.e6, b0_th=100*1.e6, d=10.6*1.e-3, D=43.1*1.e-3,**kwargs):
        super(ProtocolHCP3T, self).__init__(**kwargs)

        self.b_shell_abs_d = b_shell_abs_d
        self.b0_th = b0_th

        self.d = d
        self.D = D

        self.shell_masks = OrderedDict()
        self.gradient_scheme = None

    def load_gradient_scheme(self):
        """Method for acquisition protocol loading."""
        bvals_path = os.path.join(self.protocol_path, 'bvals')
        bvecs_path = os.path.join(self.protocol_path, 'bvecs')

        with open(bvals_path) as f:
            bvals_str = f.readlines()[0]
        b_vals = np.asarray([float(b) for b in str.split(bvals_str.strip())])

        b_vecs = np.zeros((len(b_vals), 3))
        with open(bvecs_path) as f:
            bvecs_str = f.readlines()
            for l_idx, l in enumerate(bvecs_str):
                b_vecs[:, l_idx] = np.asarray([float(b) for b in str.split(l.strip())])[:]

        d, D = np.ones(len(b_vals)) * self.d, np.ones(len(b_vals)) * self.D

        b_vals_cp = np.copy(b_vals)
        b_vals_cp[b_vals_cp < self.b0_th / 1.e6] = 0
        self.shell_masks = OrderedDict()
        while np.sum(b_vals_cp) > 0:
            bv_max = np.max(b_vals_cp)
            self.shell_masks[bv_max] = np.where((bv_max - b_vals_cp) / bv_max < self.b_shell_rel_d)[0]
            b_vals_cp[self.shell_masks[bv_max]] = 0
        self.shell_masks = OrderedDict(sorted(self.shell_masks.items()))

        if self.protocol_lib == 'dipy':
            self.gradient_scheme = gradient_table(b_vals, b_vecs, D, d, self.b0_th/1.e6)
        elif self.protocol_lib == 'dmipy':
            self.gradient_scheme = acquisition_scheme_from_bvalues(b_vals * 1.e6, b_vecs, d, D,
                                                                   self.b_shell_abs_d, self.b0_th)

    def name(self):
        """Method that returns object's name."""
        return "%s()" % type(self).__name__
