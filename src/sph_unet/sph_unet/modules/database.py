"""Class for diffusion MRI data management."""


class Database(object):
    """Class for diffusion MRI data management."""

    """
        Attributes:
            db_path (str): path to the database

        Methods:
            get_database_path: gets path to database
            set_database_path: sets path to database
            get_dmri_template_path: gets template path to the dMRI data
            set_dmri_template_path: sets template path to the dMRI data
            name: returns object's name
    """
    def __init__(self, db_path=None):
        self.db_path = db_path

    def get_database_path(self):
        """Gets database path."""
        """
            Returns:
                (str): path to the database
        """
        return self.db_path

    def set_database_path(self, db_path):
        """Sets database path."""
        """
            Arguments:
                db_path (str): path to the database
        """
        self.db_path = db_path

    def get_dmri_template_path(self):
        """Gets template path to the dMRI data and protocol."""
        """
            Returns:
                str: template path to the dMRI data and protocol
        """
        raise NotImplementedError()

    def set_dmri_template_path(self, template_iter):
        """Sets template path to the dMRI data and protocol."""
        """
            Arguments:
                template_iter (str): relative template path
                    to the dMRI data and protocol with respect
                    to database path
        """
        raise NotImplementedError()

    @property
    def name(self):
        """Returns object's name."""
        raise NotImplementedError()
