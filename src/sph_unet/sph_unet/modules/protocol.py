"""Class for dMRI acquisition protocol management."""

PI = 3.141592653589793      # pi constant
GM_RATIO_HZ = 42.58 * 1.e6  # gyromagnetic ration [Hz/T]


class Protocol(object):
    """Class for dMRI acquisition protocol management."""

    """
        Attributes:
            protocol_path (str): path to the protocol
            protocol_lib (str): library to use to create gradient table;
                options: 'dipy'/'dmipy'
            gm_ratio (float): gyro-magnetic ratio
            b0_th (float): threshold below which b values are consider to be 0
            b_shell_abs_d (float): minimum b shell absolute distance
            b_shell_rel_d (float): minimum b shell relative distance

        Methods:
            set_acquisition_path: sets protocol/acquisition path
            load_gradient_scheme: loads acquisition protocol 
            name: returns object's name including parameters
    """
    def __init__(self, protocol_path=None, protocol_lib='dipy',
                 gm_ratio=2*PI*GM_RATIO_HZ,
                 b0_th=10*1.e6, b_shell_abs_d=40*1.e6, b_shell_rel_d=0.05):

        self.protocol_path = protocol_path
        self.protocol_lib = protocol_lib

        self.gm_ratio = gm_ratio

        self.b0_th = b0_th
        self.b_shell_abs_d = b_shell_abs_d
        self.b_shell_rel_d = b_shell_rel_d

    def set_acquisition_path(self, protocol_path):
        """Sets acquisition protocol path."""
        """
            Arguments:
                protocol_path (str): path to the acquisition protocol
        """
        self.protocol_path = protocol_path

    def load_gradient_scheme(self):
        """Loads acquisition protocol table."""
        raise NotImplementedError()

    def name(self):
        """Returns object's name including parameters."""
        raise NotImplementedError()


