"""Class for dMRI descriptor (micro-structure parameters, fODF) estimation."""

import os
import numpy as np


class Estimator(object):
    """Class for dMRI descriptor (micro-structure parameters, fODF)
        estimation."""
    """
        Attributes:
            exp_out (str): path where to store model and results
            estim_source_dir (str): path to the estimators' source directory 

            signal_domain (str): signal domain that will be used as input to
                the estimator; options: raw/sph
            gt_type (str): ground truth to be estimated
            
        Methods:
            set_exp_out_path: sets path to the experiment output
            get_exp_out_path: gets path to the experiment output
            set_estimators_source_dir: set path to the estimators' source
                directory
            get_estimators_source_dir: get path to the estimators' source
                directory
            train_and_valid: runs training and validation of the model
            test: runs testing of the model
            save_model: method for model saving
            restore_model: method for model restoring
            name: returns object's name
    """
    def __init__(self, exp_out=None, estim_source_dir=None,
                 gt_type=None, signal_domain=None):
        self.exp_out = exp_out
        self.estim_source_dir = estim_source_dir
        self.gt_type = gt_type
        self.signal_domain = signal_domain

    def get_exp_out_path(self):
        """Gets experiment output path."""
        """
            Returns:
                (str): path to the experiment output
        """
        return self.exp_out

    def set_exp_out_path(self, exp_out):
        """Sets experiment output path."""
        """
            Arguments:
                exp_out_path (str): path to the experiment output
        """
        self.exp_out = exp_out

    def get_estimators_source_dir(self):
        """Gets estimators' source directory path."""
        """
            Returns:
                (str): estimators' directory path
        """
        return self.estim_source_dir

    def set_estimators_source_dir(self, source_dir):
        """Sets estimators' source directory path."""
        """
            Arguments:
                source_dir (str): entire module's source directory path
        """

        self.estim_source_dir = os.path.join(source_dir,
                                             'sph_unet', 'sph_unet', 'modules',
                                             'estimators')

    @staticmethod
    def _load_data(db, prep, data_list):
        count = 0
        for f in data_list:
            n_vox_path = os.path.join(prep.exp_out, db.name, prep.name,
                                      'n_vox_' + str(f))
            with open(n_vox_path, 'r') as f_vox:
                count += int(f_vox.readline())

        data = np.zeros((count, prep.n_in_len, prep.n_in_ch),
                        dtype=prep.data_type)
        gt = np.zeros((count, prep.n_out_len, prep.n_out_ch),
                      dtype=np.float32)
        count = 0
        for f in data_list:
            data_f = np.load(os.path.join(prep.exp_out, db.name,
                                          prep.name,
                                          'data_' + str(f) + '.npy'))
            gt_f = np.load(os.path.join(prep.exp_out, db.name,
                                        'ground_truth',
                                        prep.gt_type + '_' + str(f) + '.npy'))
            data[count:count + data_f.shape[0], :, :] = data_f
            gt[count:count + data_f.shape[0], :, :] = gt_f
            count += gt_f.shape[0]
        return data, gt

    def train_and_valid(self, prt, db):
        """Runs training and validation of the model."""
        """
            Arguments:
                prt (Protocol instance): protocol object
                db (Database instance): database object
        """
        raise NotImplementedError()

    def test(self, prt, db, epoch=None):
        """Runs testing of the model (for a given epoch)."""
        """
            Arguments:
                prt (Protocol instance): protocol object
                db (Database instance): database object
                epoch (int): ordinal number of the epoch of model training
        """
        raise NotImplementedError()

    def save_model(self, output_dir, epoch):
        """Saves model after specified epoch."""
        """
            Arguments:
                output_dir (str): path where to save the model
                epoch (int): ordinal number of epoch
        """
        model_path = os.path.join(output_dir, 'model_' + str(epoch))
        if not os.path.join(output_dir):
            os.makedirs(output_dir)
        self.saver.save(self.sess, model_path)

    def restore_model(self, input_dir, epoch):
        """Restores model after specified epoch."""
        """
            Arguments:
                input_dir (str): path where the model is saved
                epoch (int): ordinal number of epoch
        """
        model_path = os.path.join(input_dir, 'model_' + str(epoch))
        if os.path.exists(model_path + '.meta'):
            self.saver.restore(self.sess, model_path)

    @property
    def name(self):
        """Returns object's name."""
        raise NotImplementedError()

