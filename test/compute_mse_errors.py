import os
import numpy as np
import nibabel as nib
from collections import OrderedDict
from dipy.core.gradients import gradient_table

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.ticker import FormatStrFormatter
import matplotlib as mpl


d_ = 10.6 * 1.e-3
D_ = 43.1 * 1.e-3
b0_th = 100*1.e6
b_shell_rel_d = 0.05


def load_gradient_scheme(protocol_path):
    """Method for acquisition protocol loading."""
    bvals_path = os.path.join(protocol_path, 'bvals')
    bvecs_path = os.path.join(protocol_path, 'bvecs')

    with open(bvals_path) as f:
        bvals_str = f.readlines()[0]
    b_vals = np.asarray([float(b) for b in str.split(bvals_str.strip())])

    b_vecs = np.zeros((len(b_vals), 3))
    with open(bvecs_path) as f:
        bvecs_str = f.readlines()
        for l_idx, l in enumerate(bvecs_str):
            b_vecs[:, l_idx] = np.asarray([float(b) for b in str.split(l.strip())])[:]

    d, D = np.ones(len(b_vals)) * d_, np.ones(len(b_vals)) * D_

    b_vals_cp = np.copy(b_vals)
    b_vals_cp[b_vals_cp < b0_th / 1.e6] = 0
    shell_masks = OrderedDict()
    while np.sum(b_vals_cp) > 0:
        bv_max = np.max(b_vals_cp)
        shell_masks[bv_max] = np.where((bv_max - b_vals_cp) / bv_max < b_shell_rel_d)[0]
        b_vals_cp[shell_masks[bv_max]] = 0
    shell_masks = OrderedDict(sorted(shell_masks.items()))

    gradient_scheme = gradient_table(b_vals, b_vecs, D, d, b0_th / 1.e6)

    return shell_masks, gradient_scheme


def load_mask(hcp_db_path, db_20_path, subject, data_name):

    data_path = os.path.join(db_20_path, subject, data_name + '.nii.gz')
    tissues_path = os.path.join(hcp_db_path, subject, 'T1w', 'Diffusion', '5ttgen.nii.gz')
    protocol_path = os.path.join(db_20_path, subject)

    data = nib.load(data_path).get_data()
    tissues = nib.load(tissues_path).get_data()[::-1]
    mask = np.argmax(tissues, axis=3) == 2

    shell_masks, gradient_scheme = load_gradient_scheme(protocol_path)
    b0_mean = np.mean(data[:, :, :, gradient_scheme.b0s_mask], axis=3)

    mask = mask * (b0_mean > 100)

    return mask


def main():

    hcp_database_path = '/user/ssedlar/home/Work/databases/HCP'
    path_to_tests = '/user/ssedlar/home/Work/exp_out/CDMRI/'

    mse_real = np.zeros((5, 6))
    mse_syn1 = np.zeros((5, 6))
    mse_syn2 = np.zeros((5, 6))

    data_20_real_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/data_20'
    data_20_syn1_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn1/data_20'
    data_20_syn2_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn2/data_20'

    subjects = sorted(os.listdir(data_20_real_path))
    for s_idx, s in enumerate(subjects):

        mask_real = load_mask(hcp_database_path, data_20_real_path, s, 'data')
        mask_syn1 = load_mask(hcp_database_path, data_20_syn1_path, s, 'data_SNR=18')
        mask_syn2 = load_mask(hcp_database_path, data_20_syn2_path, s, 'data_SNR=18')

        gt_real_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/ground_truth/wm_fod_{}.npy'.format(40 + s_idx)
        gt_syn1_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn1/ground_truth/wm_fod_{}.npy'.format(40 + s_idx)
        gt_syn2_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn2/ground_truth/wm_fod_{}.npy'.format(40 + s_idx)

        gt_real = np.load(gt_real_path)
        gt_syn1 = np.load(gt_syn1_path)
        gt_syn2 = np.load(gt_syn2_path)

        for p_idx, p in enumerate([20, 30, 40, 60, 90, 120]):

            print(s, p)

            estim_real_MSMT_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/MSMT_CSD/{}/{}/wm_fod.nii.gz'.format(p, s)
            estim_real_3DCNN_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/3DCNN/{}/test_100/{}/estim.npy'.format(p, s)
            estim_real_Unet_1vox_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/Unet_1vox/{}/test_100/{}/estim.npy'.format(p, s)
            estim_real_Unet_s_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/Unet_s/{}/test_100/{}/estim.npy'.format(p, s)
            estim_real_Unet_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/Unet/{}/test_100/{}/estim.npy'.format(p, s)

            estim_syn1_MSMT_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn1/MSMT_CSD/{}/{}/wm_fod_noise.nii.gz'.format(p, s)
            estim_syn1_3DCNN_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn1/3DCNN/{}/test_100/{}/estim.npy'.format(p, s)
            estim_syn1_Unet_1vox_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn1/Unet_1vox/{}/test_100/{}/estim.npy'.format(p, s)
            estim_syn1_Unet_s_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn1/Unet_s/{}/test_100/{}/estim.npy'.format(p, s)
            estim_syn1_Unet_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn1/Unet/{}/test_100/{}/estim.npy'.format(p, s)

            estim_syn2_MSMT_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn2/MSMT_CSD/{}/{}/wm_fod_noise.nii.gz'.format(p, s)
            estim_syn2_3DCNN_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn2/3DCNN/{}/test_100/{}/estim.npy'.format(p, s)
            estim_syn2_Unet_1vox_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn2/Unet_1vox/{}/test_100/{}/estim.npy'.format(p, s)
            estim_syn2_Unet_s_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn2/Unet_s/{}/test_100/{}/estim.npy'.format(p, s)
            estim_syn2_Unet_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn2/Unet/{}/test_100/{}/estim.npy'.format(p, s)

            estim_real_MSMT = nib.load(estim_real_MSMT_path).get_data()[mask_real, :]
            estim_real_3DCNN = np.load(estim_real_3DCNN_path)
            estim_real_Unet_1vox = np.load(estim_real_Unet_1vox_path)
            estim_real_Unet_s = np.load(estim_real_Unet_s_path)
            estim_real_Unet = np.load(estim_real_Unet_path)

            estim_syn1_MSMT = nib.load(estim_syn1_MSMT_path).get_data()[mask_syn1, :]
            estim_syn1_3DCNN = np.load(estim_syn1_3DCNN_path)
            estim_syn1_Unet_1vox = np.load(estim_syn1_Unet_1vox_path)
            estim_syn1_Unet_s = np.load(estim_syn1_Unet_s_path)
            estim_syn1_Unet = np.load(estim_syn1_Unet_path)

            estim_syn2_MSMT = nib.load(estim_syn2_MSMT_path).get_data()[mask_syn2, :]
            estim_syn2_3DCNN = np.load(estim_syn2_3DCNN_path)
            estim_syn2_Unet_1vox = np.load(estim_syn2_Unet_1vox_path)
            estim_syn2_Unet_s = np.load(estim_syn2_Unet_s_path)
            estim_syn2_Unet = np.load(estim_syn2_Unet_path)

            mse_real[0, p_idx] += np.mean(np.sum((estim_real_MSMT - gt_real[:, :, 0]) ** 2, axis=1))
            mse_real[1, p_idx] += np.mean(np.sum((estim_real_3DCNN[:, :, 0] - gt_real[:, :, 0]) ** 2, axis=1))
            mse_real[2, p_idx] += np.mean(np.sum((estim_real_Unet_1vox[:, :, 0] - gt_real[:, :, 0]) ** 2, axis=1))
            mse_real[3, p_idx] += np.mean(np.sum((estim_real_Unet_s[:, :, 0] - gt_real[:, :, 0]) ** 2, axis=1))
            mse_real[4, p_idx] += np.mean(np.sum((estim_real_Unet[:, :, 0] - gt_real[:, :, 0]) ** 2, axis=1))

            mse_syn1[0, p_idx] += np.mean(np.sum((estim_syn1_MSMT - gt_syn1[:, :, 0]) ** 2, axis=1))
            mse_syn1[1, p_idx] += np.mean(np.sum((estim_syn1_3DCNN[:, :, 0] - gt_syn1[:, :, 0]) ** 2, axis=1))
            mse_syn1[2, p_idx] += np.mean(np.sum((estim_syn1_Unet_1vox[:, :, 0] - gt_syn1[:, :, 0]) ** 2, axis=1))
            mse_syn1[3, p_idx] += np.mean(np.sum((estim_syn1_Unet_s[:, :, 0] - gt_syn1[:, :, 0]) ** 2, axis=1))
            mse_syn1[4, p_idx] += np.mean(np.sum((estim_syn1_Unet[:, :, 0] - gt_syn1[:, :, 0]) ** 2, axis=1))

            mse_syn2[0, p_idx] += np.mean(np.sum((estim_syn2_MSMT - gt_syn2[:, :, 0]) ** 2, axis=1))
            mse_syn2[1, p_idx] += np.mean(np.sum((estim_syn2_3DCNN[:, :, 0] - gt_syn2[:, :, 0]) ** 2, axis=1))
            mse_syn2[2, p_idx] += np.mean(np.sum((estim_syn2_Unet_1vox[:, :, 0] - gt_syn2[:, :, 0]) ** 2, axis=1))
            mse_syn2[3, p_idx] += np.mean(np.sum((estim_syn2_Unet_s[:, :, 0] - gt_syn2[:, :, 0]) ** 2, axis=1))
            mse_syn2[4, p_idx] += np.mean(np.sum((estim_syn2_Unet[:, :, 0] - gt_syn2[:, :, 0]) ** 2, axis=1))

    mse_real /= 10
    mse_syn1 /= 10
    mse_syn2 /= 10

    points = [20, 30, 40, 60, 90, 120]
    fig, ax = plt.subplots(nrows=1, ncols=3)
    ax[0].plot(points, mse_real[0, :], linestyle='-', marker='o', color='royalblue', linewidth=3)
    ax[0].plot(points, mse_real[1, :], linestyle='-', marker='o', color='orangered', linewidth=3)
    ax[0].plot(points, mse_real[2, :], linestyle='-', marker='o', color='blueviolet', linewidth=3)
    ax[0].plot(points, mse_real[3, :], linestyle='-', marker='o', color='yellowgreen', linewidth=3)
    ax[0].plot(points, mse_real[4, :], linestyle='-', marker='o', color='k', linewidth=3)
    ax[0].set_xlabel('$N_{DWI}$', fontsize=16)
    ax[0].set_ylabel('MSE', fontsize=16)
    ax[0].set_ylim([0, 0.042])
    ax[0].set_xticks(points)
    ax[0].set_title('Real dataset', fontsize=16)
    ax[0].grid()

    ax[1].plot(points, mse_syn1[0, :], linestyle='-', marker='o', color='royalblue', linewidth=3)
    ax[1].plot(points, mse_syn1[1, :], linestyle='-', marker='o', color='orangered', linewidth=3)
    ax[1].plot(points, mse_syn1[2, :], linestyle='-', marker='o', color='blueviolet', linewidth=3)
    ax[1].plot(points, mse_syn1[3, :], linestyle='-', marker='o', color='yellowgreen', linewidth=3)
    ax[1].plot(points, mse_syn1[4, :], linestyle='-', marker='o', color='k', linewidth=3)
    ax[1].set_xlabel('$N_{DWI}$', fontsize=16)
    ax[1].set_ylim([0, 0.042])
    ax[1].set_yticklabels([])
    ax[1].set_xticks(points)
    ax[1].set_title('Synthetic dataset 1', fontsize=16)
    ax[1].grid()

    ax[2].plot(points, mse_syn2[0, :], linestyle='-', marker='o', color='royalblue', linewidth=3)
    ax[2].plot(points, mse_syn2[1, :], linestyle='-', marker='o', color='orangered', linewidth=3)
    ax[2].plot(points, mse_syn2[2, :], linestyle='-', marker='o', color='blueviolet', linewidth=3)
    ax[2].plot(points, mse_syn2[3, :], linestyle='-', marker='o', color='yellowgreen', linewidth=3)
    ax[2].plot(points, mse_syn2[4, :], linestyle='-', marker='o', color='k', linewidth=3)
    ax[2].set_xlabel('$N_{DWI}$', fontsize=16)
    ax[2].set_ylim([0, 0.042])
    ax[2].set_yticklabels([])
    ax[2].set_xticks(points)
    ax[2].grid()
    ax[2].set_title('Synthetic dataset 2', fontsize=16)
    ax[2].legend(['MSMT-CSD', '3DCNN', '$S^2Unet^{1x1x1}$', '$S^2Unet^{3x3x3}_s$', '$S^2Unet^{3x3x3}$'], loc="upper right", fontsize=16)
    plt.show()


if __name__ == '__main__':
    main()
