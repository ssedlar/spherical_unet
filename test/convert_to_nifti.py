import os
import numpy as np
import nibabel as nib
from collections import OrderedDict
from dipy.core.gradients import gradient_table

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.ticker import FormatStrFormatter
import matplotlib as mpl
import nibabel as nib


d_ = 10.6 * 1.e-3
D_ = 43.1 * 1.e-3
b0_th = 100*1.e6
b_shell_rel_d = 0.05


def load_gradient_scheme(protocol_path):
    """Method for acquisition protocol loading."""
    bvals_path = os.path.join(protocol_path, 'bvals')
    bvecs_path = os.path.join(protocol_path, 'bvecs')

    with open(bvals_path) as f:
        bvals_str = f.readlines()[0]
    b_vals = np.asarray([float(b) for b in str.split(bvals_str.strip())])

    b_vecs = np.zeros((len(b_vals), 3))
    with open(bvecs_path) as f:
        bvecs_str = f.readlines()
        for l_idx, l in enumerate(bvecs_str):
            b_vecs[:, l_idx] = np.asarray([float(b) for b in str.split(l.strip())])[:]

    d, D = np.ones(len(b_vals)) * d_, np.ones(len(b_vals)) * D_

    b_vals_cp = np.copy(b_vals)
    b_vals_cp[b_vals_cp < b0_th / 1.e6] = 0
    shell_masks = OrderedDict()
    while np.sum(b_vals_cp) > 0:
        bv_max = np.max(b_vals_cp)
        shell_masks[bv_max] = np.where((bv_max - b_vals_cp) / bv_max < b_shell_rel_d)[0]
        b_vals_cp[shell_masks[bv_max]] = 0
    shell_masks = OrderedDict(sorted(shell_masks.items()))

    gradient_scheme = gradient_table(b_vals, b_vecs, D, d, b0_th / 1.e6)

    return shell_masks, gradient_scheme


def load_mask(db_path, subject):

    data_path = os.path.join(db_path, subject, 'data_SNR=18.nii.gz')
    tissues_path = os.path.join('/user/ssedlar/home/Work/databases/HCP', subject, 'T1w', 'Diffusion', '5ttgen.nii.gz')
    protocol_path = os.path.join(db_path, subject)

    data = nib.load(data_path).get_data()
    tissues = nib.load(tissues_path).get_data()[::-1]
    mask = np.argmax(tissues, axis=3) == 2

    shell_masks, gradient_scheme = load_gradient_scheme(protocol_path)
    b0_mean = np.mean(data[:, :, :, gradient_scheme.b0s_mask], axis=3)

    mask = mask * (b0_mean > 100)

    return mask


def main():

    db = 'Syn2'

    orig_data_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn2/MSMT_CSD/20/'
    path_to_tests = '/user/ssedlar/home/Work/exp_out/CDMRI/' + db

    subjects = sorted(os.listdir(os.path.join(path_to_tests, '3DCNN/20/test_200/')))
    print("subjects:", subjects)

    for s_idx, s in enumerate(subjects):

        print("s:", s)
        mask = load_mask(orig_data_path, s)
        for n_p_idx, n_p in enumerate([20, 30, 40, 60, 90, 120]):

            path_to_3DCNN = os.path.join(path_to_tests, '3DCNN', str(n_p), 'test_200', s)
            path_to_Unet = os.path.join(path_to_tests, 'Unet', str(n_p), 'test_200', s)
            path_to_Unet_vox1 = os.path.join(path_to_tests, 'Unet_1vox', str(n_p), 'test_200', s)

            path_to_msmt = os.path.join(path_to_tests, 'MSMT_CSD', str(n_p), s, 'wm_fod_noise.nii.gz')

            fodf_3DCNN = np.load(os.path.join(path_to_3DCNN, 'estim.npy'))
            fodf_Unet = np.load(os.path.join(path_to_Unet, 'estim.npy'))
            fodf_Unet_vox1 = np.load(os.path.join(path_to_Unet_vox1, 'estim.npy'))


            fodf_msmt_nii = nib.load(path_to_msmt)

            header_new = fodf_msmt_nii.header.copy()
            affine_new = fodf_msmt_nii.affine.copy()

            fod_3DCNN_np = np.zeros((145, 174, 145, 45))
            fod_3DCNN_np[mask, :] = fodf_3DCNN[:, :, 0]
            fodf_3DCNN_nii = nib.nifti1.Nifti1Image(fod_3DCNN_np, affine=affine_new, header=header_new)
            nib.save(fodf_3DCNN_nii, os.path.join(path_to_3DCNN, 'wm_fod.nii.gz'))

            fod_Unet_np = np.zeros((145, 174, 145, 45))
            fod_Unet_np[mask, :] = fodf_Unet[:, :, 0]
            fodf_Unet_nii = nib.nifti1.Nifti1Image(fod_Unet_np, affine=affine_new, header=header_new)
            nib.save(fodf_Unet_nii, os.path.join(path_to_Unet, 'wm_fod.nii.gz'))

            fod_Unet_vox1_np = np.zeros((145, 174, 145, 45))
            fod_Unet_vox1_np[mask, :] = fodf_Unet_vox1[:, :, 0]
            fodf_Unet_vox1_nii = nib.nifti1.Nifti1Image(fod_Unet_vox1_np, affine=affine_new, header=header_new)
            nib.save(fodf_Unet_vox1_nii, os.path.join(path_to_Unet_vox1, 'wm_fod.nii.gz'))



if __name__ == '__main__':
    main()
