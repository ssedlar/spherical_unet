import os
import numpy as np
import nibabel as nib


def peaks_process(peaks_in):

    peaks = np.zeros((peaks_in.shape[0], 3, 3))

    peaks[:, 0, :] = peaks_in[:, 0:3]
    peaks[:, 1, :] = peaks_in[:, 3:6]
    peaks[:, 2, :] = peaks_in[:, 6:9]

    peaks_amp = np.sqrt(np.sum(peaks**2, axis=2))

    return peaks, peaks_amp

def two_peaks_mask(peaks_a):

    two_peaks_mask = (0.1 * peaks_a[:, 0] < peaks_a[:, 1]) * (0.1 * peaks_a[:, 0] > peaks_a[:, 2])

    return two_peaks_mask

def two_peaks_angular_error(gt_dir, gt_a, comp_dir, comp_a, mask_gt, mask_comp):

        mask = mask_gt * mask_comp

        cosine_sim_gt = np.sum(gt_dir[mask, 0, :] * gt_dir[mask, 1, :], axis=1)/(gt_a[mask, 0] * gt_a[mask, 1])
        cosine_sim_c = np.sum(comp_dir[mask, 0, :] * comp_dir[mask, 1, :], axis=1)/(comp_a[mask, 0] * comp_a[mask, 1])

        angle_gt = np.abs(np.arccos(cosine_sim_gt)) * 180/np.pi
        conv_180 = angle_gt > 90
        angle_gt[conv_180] = 180 - angle_gt[conv_180]

        angle_c = np.abs(np.arccos(cosine_sim_c)) * 180/np.pi
        conv_180 = angle_c > 90
        angle_c[conv_180] = 180 - angle_c[conv_180]

        angular_error = np.abs(angle_gt - angle_c)
        return angular_error, angle_gt


def main():

    subjects = sorted(os.listdir('/user/ssedlar/home/Work/exp_out/CDMRI/Real/3DCNN/20/test_200'))
    gt_template_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/ground_truth/{}/peaks.nii.gz'
    mask_template_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/ground_truth/{}/mask.npy'

    msmt_template_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/MSMT_CSD/{}/{}/peaks.nii.gz'
    cnn_template_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/3DCNN/{}/test_200/{}/peaks.nii.gz'
    unet_template_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/Unet/{}/test_200/{}/peaks.nii.gz'
    unet_vox_template_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/Unet_1vox/{}/test_200/{}/peaks.nii.gz'

    ae_two = {}
    ae_gt = {}

    for p_idx, p in enumerate([20, 30, 40, 60, 90, 120]):
        print("p:", p)
        if p not in ae_two:
            ae_two[p] = {}
            ae_gt[p] = {}

        for s in subjects:
            print(s)
            if s not in ae_two[p]:
                ae_two[p][s] = {}
                ae_gt[p][s] = {}

            mask = np.load(mask_template_path.format(s))
            gt_peaks = nib.load(gt_template_path.format(s)).get_data()[mask==1, :]

            msmt_peaks = nib.load(msmt_template_path.format(p, s)).get_data()[mask==1, :]
            cnn_peaks = nib.load(cnn_template_path.format(p, s)).get_data()[mask==1, :]
            unet_peaks = nib.load(unet_template_path.format(p, s)).get_data()[mask==1, :]
            unet_vox_peaks = nib.load(unet_vox_template_path.format(p, s)).get_data()[mask==1, :]

            gt_peaks_d, gt_peaks_a = peaks_process(gt_peaks)
            msmt_peaks_d, msmt_peaks_a = peaks_process(msmt_peaks)
            cnn_peaks_d, cnn_peaks_a = peaks_process(cnn_peaks)
            unet_peaks_d, unet_peaks_a = peaks_process(unet_peaks)
            unet_vox_peaks_d, unet_vox_peaks_a = peaks_process(unet_vox_peaks)

            gt_two_peaks = two_peaks_mask(gt_peaks_a)
            msmt_two_peaks = two_peaks_mask(cnn_peaks_a)
            cnn_two_peaks = two_peaks_mask(cnn_peaks_a)
            unet_two_peaks = two_peaks_mask(unet_peaks_a)
            unet_vox_two_peaks = two_peaks_mask(unet_vox_peaks_a)

            ae_two[p][s]['msmt'], ae_gt[p][s]['msmt'] = two_peaks_angular_error(gt_peaks_d, gt_peaks_a, msmt_peaks_d, msmt_peaks_a,
                                                        gt_two_peaks, msmt_two_peaks)

            ae_two[p][s]['cnn'], ae_gt[p][s]['cnn'] = two_peaks_angular_error(gt_peaks_d, gt_peaks_a, cnn_peaks_d, cnn_peaks_a,
                                                        gt_two_peaks, cnn_two_peaks)

            ae_two[p][s]['unet'], ae_gt[p][s]['unet'] = two_peaks_angular_error(gt_peaks_d, gt_peaks_a, unet_peaks_d, unet_peaks_a,
                                                        gt_two_peaks, unet_two_peaks)

            ae_two[p][s]['unet_vox'], ae_gt[p][s]['unet_vox'] = two_peaks_angular_error(gt_peaks_d, gt_peaks_a, unet_vox_peaks_d, unet_vox_peaks_a,
                                                        gt_two_peaks, unet_vox_two_peaks)

    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(nrows=2, ncols=3)
    ps = [20, 30, 40, 60, 90, 120]
    for i in range(6):
        row = i // 3
        column = i % 3

        errors_msmt = np.zeros(9)
        errors_cnn = np.zeros(9)
        errors_unet = np.zeros(9)
        errors_unet_vox = np.zeros(9)
        for s_idx, s in enumerate(subjects):
            bl = 0

            for ca_idx, ca in enumerate([10, 20, 30, 40, 50, 60, 70, 80, 90]):
                mask = (ae_gt[ps[i]][s]['msmt'] > bl) * (ae_gt[ps[i]][s]['msmt'] <= ca)
                errors_msmt[ca_idx] += np.mean(ae_two[ps[i]][s]['msmt'][mask])

                mask = (ae_gt[ps[i]][s]['cnn'] > bl) * (ae_gt[ps[i]][s]['cnn'] <= ca)
                errors_cnn[ca_idx] += np.mean(ae_two[ps[i]][s]['cnn'][mask])

                mask = (ae_gt[ps[i]][s]['unet'] > bl) * (ae_gt[ps[i]][s]['unet'] <= ca)
                errors_unet[ca_idx] += np.mean(ae_two[ps[i]][s]['unet'][mask])

                mask = (ae_gt[ps[i]][s]['unet_vox'] > bl) * (ae_gt[ps[i]][s]['unet_vox'] <= ca)
                errors_unet_vox[ca_idx] += np.mean(ae_two[ps[i]][s]['unet_vox'][mask])
                bl = ca

        ax[row][column].plot(errors_msmt / 10, 'r')
        ax[row][column].plot(errors_cnn / 10, 'g')
        ax[row][column].plot(errors_unet_vox / 10, 'b')
        ax[row][column].plot(errors_unet / 10, 'k')

    plt.show()

if __name__ == '__main__':
    main()