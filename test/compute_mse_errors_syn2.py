import os
import numpy as np
import nibabel as nib
from collections import OrderedDict
from dipy.core.gradients import gradient_table

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.ticker import FormatStrFormatter
import matplotlib as mpl


d_ = 10.6 * 1.e-3
D_ = 43.1 * 1.e-3
b0_th = 100*1.e6
b_shell_rel_d = 0.05


def load_gradient_scheme(protocol_path):
    """Method for acquisition protocol loading."""
    bvals_path = os.path.join(protocol_path, 'bvals')
    bvecs_path = os.path.join(protocol_path, 'bvecs')

    with open(bvals_path) as f:
        bvals_str = f.readlines()[0]
    b_vals = np.asarray([float(b) for b in str.split(bvals_str.strip())])

    b_vecs = np.zeros((len(b_vals), 3))
    with open(bvecs_path) as f:
        bvecs_str = f.readlines()
        for l_idx, l in enumerate(bvecs_str):
            b_vecs[:, l_idx] = np.asarray([float(b) for b in str.split(l.strip())])[:]

    d, D = np.ones(len(b_vals)) * d_, np.ones(len(b_vals)) * D_

    b_vals_cp = np.copy(b_vals)
    b_vals_cp[b_vals_cp < b0_th / 1.e6] = 0
    shell_masks = OrderedDict()
    while np.sum(b_vals_cp) > 0:
        bv_max = np.max(b_vals_cp)
        shell_masks[bv_max] = np.where((bv_max - b_vals_cp) / bv_max < b_shell_rel_d)[0]
        b_vals_cp[shell_masks[bv_max]] = 0
    shell_masks = OrderedDict(sorted(shell_masks.items()))

    gradient_scheme = gradient_table(b_vals, b_vecs, D, d, b0_th / 1.e6)

    return shell_masks, gradient_scheme


def load_mask(db_path, subject):

    data_path = os.path.join(db_path, subject, 'data_SNR=18.nii.gz')
    tissues_path = os.path.join('/user/ssedlar/home/Work/databases/HCP/{}/T1w/Diffusion'.format(subject), '5ttgen.nii.gz')
    protocol_path = os.path.join(db_path, subject)

    data = nib.load(data_path).get_data()
    tissues = nib.load(tissues_path).get_data()[::-1]
    mask = np.argmax(tissues, axis=3) == 2

    shell_masks, gradient_scheme = load_gradient_scheme(protocol_path)
    b0_mean = np.mean(data[:, :, :, gradient_scheme.b0s_mask], axis=3)

    mask = mask * (b0_mean > 100)

    return mask


def main():

    db = 'Syn2'

    orig_data_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn2/MSMT_CSD/20/'
    path_to_tests = '/user/ssedlar/home/Work/exp_out/CDMRI/' + db

    subjects = sorted(os.listdir(os.path.join(path_to_tests, '3DCNN/20/test_200/')))
    print("subjects:", subjects)

    errors_mean = np.zeros((4, 6))
    errors_std = np.zeros((4, 6))

    for s_idx, s in enumerate(subjects):

        print("s:", s)
        mask = load_mask(orig_data_path, s)
        for n_p_idx, n_p in enumerate([20, 30, 40, 60, 90, 120]):

            path_to_3DCNN = os.path.join(path_to_tests, '3DCNN', str(n_p), 'test_200', s, 'estim.npy')
            path_to_Unet = os.path.join(path_to_tests, 'Unet', str(n_p), 'test_200', s, 'estim.npy')
            path_to_Unet_vox1 = os.path.join(path_to_tests, 'Unet_1vox', str(n_p), 'test_200', s, 'estim.npy')

            path_to_gt = os.path.join(path_to_tests, 'ground_truth', 'wm_fod_' + str(40 + s_idx) + '.npy')

            path_to_msmt = os.path.join(path_to_tests, 'MSMT_CSD', str(n_p), s, 'wm_fod_noise.nii.gz')

            fodf_3DCNN = np.load(path_to_3DCNN)
            fodf_Unet = np.load(path_to_Unet)

            fodf_gt = np.load(path_to_gt)

            try:
                fodf_Unet_vox1 = np.load(path_to_Unet_vox1)
            except:
                fodf_Unet_vox1 = np.copy(fodf_gt)

            fodf_msmt_nii = nib.load(path_to_msmt)

            fodf_msmt = fodf_msmt_nii.get_data()[mask==1, :]

            errors_mean[0, n_p_idx] += np.mean(np.sum((fodf_gt[:, :, 0] - fodf_msmt) ** 2, axis=1))
            errors_std[0, n_p_idx] += np.std(np.sum((fodf_gt[:, :, 0] - fodf_msmt) ** 2, axis=1))

            errors_mean[1, n_p_idx] += np.mean(np.sum((fodf_gt[:, :, 0] - fodf_3DCNN[:, :, 0]) ** 2, axis=1))
            errors_std[1, n_p_idx] += np.std(np.sum((fodf_gt[:, :, 0] - fodf_3DCNN[:, :, 0]) ** 2, axis=1))

            errors_mean[2, n_p_idx] += np.mean(np.sum((fodf_gt[:, :, 0] - fodf_Unet_vox1[:, :, 0]) ** 2, axis=1))
            errors_std[2, n_p_idx] += np.std(np.sum((fodf_gt[:, :, 0] - fodf_Unet_vox1[:, :, 0]) ** 2, axis=1))

            errors_mean[3, n_p_idx] += np.mean(np.sum((fodf_gt[:, :, 0] - fodf_Unet[:, :, 0]) ** 2, axis=1))
            errors_std[3, n_p_idx] += np.std(np.sum((fodf_gt[:, :, 0] - fodf_Unet[:, :, 0]) ** 2, axis=1))

    errors_mean /= 10
    errors_std /= 10

    with open("results.txt", 'w') as f_r:
        for i in range(4):
            for n_p_idx, n_p in enumerate([20, 30, 40, 60, 90, 120]):
                f_r.write("%.4f %s%.4f\t" % (errors_mean[i, n_p_idx], '+/-', errors_std[i, n_p_idx]))
                f_r.write("\t")
            f_r.write("\n")



    barWidth = 0.2
    r1 = np.arange(1, 7) - barWidth
    r2 = np.arange(1, 7)
    r3 = [x + barWidth for x in r2]
    r4 = [x + barWidth for x in r3]

    fig, ax1 = plt.subplots(1, 1)

    ax1.bar(r1, errors_mean[0, :], yerr=errors_std[0, :], color='r', width=barWidth, edgecolor='white', label='MSMT_CSD')
    ax1.bar(r2, errors_mean[1, :], yerr=errors_std[1, :], color='g', width=barWidth, edgecolor='white', label='3DCNN')
    ax1.bar(r3, errors_mean[2, :], yerr=errors_std[2, :], color='b', width=barWidth, edgecolor='white', label='sphUnet_1x1x1')
    ax1.bar(r4, errors_mean[3, :], yerr=errors_std[3, :], color='y', width=barWidth, edgecolor='white', label='sphUnet_3x3x3')

    ax1.set_xticks([1, 2, 3, 4, 5, 6])
    ax1.set_xticklabels([20, 30, 40, 60, 90, 120])
    ax1.set_xlabel('Number of dMRI gradient directions (sampling points)')

    ax1.legend(['MSMT_CSD', '3DCNN', 'sphUnet_1x1x1', 'sphUnet_3x3x3'])
    ax1.set_ylabel("MSE")
    #ax1.set_ylim([0, 0.5])
    plt.show()

    print(errors_mean)
    print(errors_std)
    import sys
    sys.exit(2)

if __name__ == '__main__':
    main()
