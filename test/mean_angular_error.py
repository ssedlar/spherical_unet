import os
import numpy as np
import nibabel as nib
from collections import OrderedDict
from dipy.core.gradients import gradient_table

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.ticker import FormatStrFormatter
import matplotlib as mpl


d_ = 10.6 * 1.e-3
D_ = 43.1 * 1.e-3
b0_th = 100*1.e6
b_shell_rel_d = 0.05


def load_gradient_scheme(protocol_path):
    """Method for acquisition protocol loading."""
    bvals_path = os.path.join(protocol_path, 'bvals')
    bvecs_path = os.path.join(protocol_path, 'bvecs')

    with open(bvals_path) as f:
        bvals_str = f.readlines()[0]
    b_vals = np.asarray([float(b) for b in str.split(bvals_str.strip())])

    b_vecs = np.zeros((len(b_vals), 3))
    with open(bvecs_path) as f:
        bvecs_str = f.readlines()
        for l_idx, l in enumerate(bvecs_str):
            b_vecs[:, l_idx] = np.asarray([float(b) for b in str.split(l.strip())])[:]

    d, D = np.ones(len(b_vals)) * d_, np.ones(len(b_vals)) * D_

    b_vals_cp = np.copy(b_vals)
    b_vals_cp[b_vals_cp < b0_th / 1.e6] = 0
    shell_masks = OrderedDict()
    while np.sum(b_vals_cp) > 0:
        bv_max = np.max(b_vals_cp)
        shell_masks[bv_max] = np.where((bv_max - b_vals_cp) / bv_max < b_shell_rel_d)[0]
        b_vals_cp[shell_masks[bv_max]] = 0
    shell_masks = OrderedDict(sorted(shell_masks.items()))

    gradient_scheme = gradient_table(b_vals, b_vecs, D, d, b0_th / 1.e6)

    return shell_masks, gradient_scheme


def peaks_process(peaks_in):

    peaks = np.zeros((peaks_in.shape[0], 3, 3))

    peaks[:, 0, :] = peaks_in[:, 0:3]
    peaks[:, 1, :] = peaks_in[:, 3:6]
    peaks[:, 2, :] = peaks_in[:, 6:9]

    peaks_amp = np.sqrt(np.sum(peaks**2, axis=2))

    return peaks, peaks_amp


def single_peaks_mask(peaks_a):

    single_peaks_mask = (0.1 * peaks_a[:, 0] > peaks_a[:, 1]) * (0.1 * peaks_a[:, 0] > peaks_a[:, 2])

    return single_peaks_mask


def single_peaks_angular_error(gt_dir, gt_a, comp_dir, comp_a, mask_gt, mask_comp):

        mask = mask_gt * mask_comp

        cosine_sim = np.sum(gt_dir[mask, 0, :] * comp_dir[mask, 0, :], axis=1)/(gt_a[mask, 0] * comp_a[mask, 0] + 1.e-8)
        angular_error = np.abs(np.arccos(cosine_sim)) * 180/np.pi
        conv_180 = angular_error > 90
        angular_error[conv_180] = 180 - angular_error[conv_180]

        return np.mean(angular_error)


def load_mask(hcp_db_path, db_20_path, subject, data_name):

    data_path = os.path.join(db_20_path, subject, data_name + '.nii.gz')
    tissues_path = os.path.join(hcp_db_path, subject, 'T1w', 'Diffusion', '5ttgen.nii.gz')
    protocol_path = os.path.join(db_20_path, subject)

    data = nib.load(data_path).get_data()
    tissues = nib.load(tissues_path).get_data()[::-1]
    mask = np.argmax(tissues, axis=3) == 2

    shell_masks, gradient_scheme = load_gradient_scheme(protocol_path)
    b0_mean = np.mean(data[:, :, :, gradient_scheme.b0s_mask], axis=3)

    mask = mask * (b0_mean > 100)

    return mask


def main():

    hcp_database_path = '/user/ssedlar/home/Work/databases/HCP'
    path_to_tests = '/user/ssedlar/home/Work/exp_out/CDMRI/'

    mae_real = np.zeros((5, 6))
    mae_syn1 = np.zeros((5, 6))
    mae_syn2 = np.zeros((5, 6))

    data_20_real_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/data_20'
    data_20_syn1_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn1/data_20'
    data_20_syn2_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn2/data_20'

    subjects = sorted(os.listdir(data_20_real_path))
    for s_idx, s in enumerate(subjects):

        mask_real = load_mask(hcp_database_path, data_20_real_path, s, 'data')
        mask_syn1 = load_mask(hcp_database_path, data_20_syn1_path, s, 'data_SNR=18')
        mask_syn2 = load_mask(hcp_database_path, data_20_syn2_path, s, 'data_SNR=18')

        gt_real_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/ground_truth/{}/peaks_gt.nii.gz'.format(s)
        gt_syn1_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn1/ground_truth/{}/peaks_gt.nii.gz'.format(s)
        gt_syn2_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn2/ground_truth/{}/peaks_gt.nii.gz'.format(s)

        peaks_real_gt = nib.load(gt_real_path).get_data()[mask_real == 1, :]
        peaks_real_gt[np.isnan(peaks_real_gt)] = 0

        peaks_syn1_gt = nib.load(gt_syn1_path).get_data()[mask_syn1 == 1, :]
        peaks_syn1_gt[np.isnan(peaks_syn1_gt)] = 0

        peaks_syn2_gt = nib.load(gt_syn2_path).get_data()[mask_syn2 == 1, :]
        peaks_syn2_gt[np.isnan(peaks_syn2_gt)] = 0

        dir_real_gt, a_real_gt = peaks_process(peaks_real_gt)
        dir_syn1_gt, a_syn1_gt = peaks_process(peaks_syn1_gt)
        dir_syn2_gt, a_syn2_gt = peaks_process(peaks_syn2_gt)

        gt_real_single_peaks = single_peaks_mask(a_real_gt)
        gt_syn1_single_peaks = single_peaks_mask(a_syn1_gt)
        gt_syn2_single_peaks = single_peaks_mask(a_syn2_gt)


        for p_idx, p in enumerate([20, 30, 40, 60, 90, 120]):

            print(s, p)

            peaks_real_MSMT_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/MSMT_CSD/{}/{}/peaks.nii.gz'.format(p, s)
            peaks_real_3DCNN_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/3DCNN/{}/test_100/{}/peaks.nii.gz'.format(p, s)
            peaks_real_Unet_1vox_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/Unet_1vox/{}/test_100/{}/peaks.nii.gz'.format(p, s)
            peaks_real_Unet_s_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/Unet_s/{}/test_100/{}/peaks.nii.gz'.format(p, s)
            peaks_real_Unet_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/Unet/{}/test_100/{}/peaks.nii.gz'.format(p, s)

            peaks_syn1_MSMT_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn1/MSMT_CSD/{}/{}/peaks.nii.gz'.format(p, s)
            peaks_syn1_3DCNN_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn1/3DCNN/{}/test_100/{}/peaks.nii.gz'.format(p, s)
            peaks_syn1_Unet_1vox_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn1/Unet_1vox/{}/test_100/{}/peaks.nii.gz'.format(p, s)
            peaks_syn1_Unet_s_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn1/Unet_s/{}/test_100/{}/peaks.nii.gz'.format(p, s)
            peaks_syn1_Unet_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn1/Unet/{}/test_100/{}/peaks.nii.gz'.format(p, s)

            peaks_syn2_MSMT_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn2/MSMT_CSD/{}/{}/peaks.nii.gz'.format(p, s)
            peaks_syn2_3DCNN_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn2/3DCNN/{}/test_100/{}/peaks.nii.gz'.format(p, s)
            peaks_syn2_Unet_1vox_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn2/Unet_1vox/{}/test_100/{}/peaks.nii.gz'.format(p, s)
            peaks_syn2_Unet_s_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn2/Unet_s/{}/test_100/{}/peaks.nii.gz'.format(p, s)
            peaks_syn2_Unet_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Syn2/Unet/{}/test_100/{}/peaks.nii.gz'.format(p, s)


            peaks_real_MSMT = nib.load(peaks_real_MSMT_path).get_data()[mask_real==1, :]
            peaks_real_MSMT[np.isnan(peaks_real_MSMT)] = 0

            peaks_real_3DCNN = nib.load(peaks_real_3DCNN_path).get_data()[mask_real==1, :]
            peaks_real_3DCNN[np.isnan(peaks_real_3DCNN)] = 0

            peaks_real_Unet_1vox = nib.load(peaks_real_Unet_1vox_path).get_data()[mask_real==1, :]
            peaks_real_Unet_1vox[np.isnan(peaks_real_Unet_1vox)] = 0

            peaks_real_Unet_s = nib.load(peaks_real_Unet_s_path).get_data()[mask_real==1, :]
            peaks_real_Unet_s[np.isnan(peaks_real_Unet_s)] = 0

            peaks_real_Unet = nib.load(peaks_real_Unet_path).get_data()[mask_real==1, :]
            peaks_real_Unet[np.isnan(peaks_real_Unet)] = 0



            peaks_syn1_MSMT = nib.load(peaks_syn1_MSMT_path).get_data()[mask_syn1, :]
            peaks_syn1_MSMT[np.isnan(peaks_syn1_MSMT)] = 0

            peaks_syn1_3DCNN = nib.load(peaks_syn1_3DCNN_path).get_data()[mask_syn1, :]
            peaks_syn1_3DCNN[np.isnan(peaks_syn1_3DCNN)] = 0

            peaks_syn1_Unet_1vox = nib.load(peaks_syn1_Unet_1vox_path).get_data()[mask_syn1, :]
            peaks_syn1_Unet_1vox[np.isnan(peaks_syn1_Unet_1vox)] = 0

            peaks_syn1_Unet_s = nib.load(peaks_syn1_Unet_s_path).get_data()[mask_syn1, :]
            peaks_syn1_Unet_s[np.isnan(peaks_syn1_Unet_s)] = 0

            peaks_syn1_Unet = nib.load(peaks_syn1_Unet_path).get_data()[mask_syn1, :]
            peaks_syn1_Unet[np.isnan(peaks_syn1_Unet)] = 0


            peaks_syn2_MSMT = nib.load(peaks_syn2_MSMT_path).get_data()[mask_syn2, :]
            peaks_syn2_MSMT[np.isnan(peaks_syn2_MSMT)] = 0

            peaks_syn2_3DCNN = nib.load(peaks_syn2_3DCNN_path).get_data()[mask_syn2, :]
            peaks_syn2_3DCNN[np.isnan(peaks_syn2_3DCNN)] = 0

            peaks_syn2_Unet_1vox = nib.load(peaks_syn2_Unet_1vox_path).get_data()[mask_syn2, :]
            peaks_syn2_Unet_1vox[np.isnan(peaks_syn2_Unet_1vox)] = 0

            peaks_syn2_Unet_s = nib.load(peaks_syn2_Unet_s_path).get_data()[mask_syn2, :]
            peaks_syn2_Unet_s[np.isnan(peaks_syn2_Unet_s)] = 0

            peaks_syn2_Unet = nib.load(peaks_syn2_Unet_path).get_data()[mask_syn2, :]
            peaks_syn2_Unet[np.isnan(peaks_syn2_Unet)] = 0


            dir_real_msmt, a_real_msmt = peaks_process(peaks_real_MSMT)
            dir_real_3dcnn, a_real_3dcnn = peaks_process(peaks_real_3DCNN)
            dir_real_Unet_1vox, a_real_Unet_1vox = peaks_process(peaks_real_Unet_1vox)
            dir_real_Unet_s, a_real_Unet_s = peaks_process(peaks_real_Unet_s)
            dir_real_Unet, a_real_Unet = peaks_process(peaks_real_Unet)

            dir_syn1_msmt, a_syn1_msmt = peaks_process(peaks_syn1_MSMT)
            dir_syn1_3dcnn, a_syn1_3dcnn = peaks_process(peaks_syn1_3DCNN)
            dir_syn1_Unet_1vox, a_syn1_Unet_1vox = peaks_process(peaks_syn1_Unet_1vox)
            dir_syn1_Unet_s, a_syn1_Unet_s = peaks_process(peaks_syn1_Unet_s)
            dir_syn1_Unet, a_syn1_Unet = peaks_process(peaks_syn1_Unet)

            dir_syn2_msmt, a_syn2_msmt = peaks_process(peaks_syn2_MSMT)
            dir_syn2_3dcnn, a_syn2_3dcnn = peaks_process(peaks_syn2_3DCNN)
            dir_syn2_Unet_1vox, a_syn2_Unet_1vox = peaks_process(peaks_syn2_Unet_1vox)
            dir_syn2_Unet_s, a_syn2_Unet_s = peaks_process(peaks_syn2_Unet_s)
            dir_syn2_Unet, a_syn2_Unet = peaks_process(peaks_syn2_Unet)


            msmt_real_single_peaks = single_peaks_mask(a_real_msmt)
            cnn_real_single_peaks = single_peaks_mask(a_real_3dcnn)
            Unet_1vox_real_single_peaks = single_peaks_mask(a_real_Unet_1vox)
            Unet_s_real_single_peaks = single_peaks_mask(a_real_Unet_s)
            Unet_real_single_peaks = single_peaks_mask(a_real_Unet)

            msmt_syn1_single_peaks = single_peaks_mask(a_syn1_msmt)
            cnn_syn1_single_peaks = single_peaks_mask(a_syn1_3dcnn)
            Unet_1vox_syn1_single_peaks = single_peaks_mask(a_syn1_Unet_1vox)
            Unet_s_syn1_single_peaks = single_peaks_mask(a_syn1_Unet_s)
            Unet_syn1_single_peaks = single_peaks_mask(a_syn1_Unet)

            msmt_syn2_single_peaks = single_peaks_mask(a_syn2_msmt)
            cnn_syn2_single_peaks = single_peaks_mask(a_syn2_3dcnn)
            Unet_1vox_syn2_single_peaks = single_peaks_mask(a_syn2_Unet_1vox)
            Unet_s_syn2_single_peaks = single_peaks_mask(a_syn2_Unet_s)
            Unet_syn2_single_peaks = single_peaks_mask(a_syn2_Unet)

            mae_real[0, p_idx] += single_peaks_angular_error(dir_real_gt, a_real_gt,
                                                            dir_real_msmt, a_real_msmt,
                                                            gt_real_single_peaks, msmt_real_single_peaks)
            mae_real[1, p_idx] += single_peaks_angular_error(dir_real_gt, a_real_gt,
                                                            dir_real_3dcnn, a_real_3dcnn,
                                                            gt_real_single_peaks, cnn_real_single_peaks)
            mae_real[2, p_idx] += single_peaks_angular_error(dir_real_gt, a_real_gt,
                                                            dir_real_Unet_1vox, a_real_Unet_1vox,
                                                            gt_real_single_peaks, Unet_1vox_real_single_peaks)
            mae_real[3, p_idx] += single_peaks_angular_error(dir_real_gt, a_real_gt,
                                                            dir_real_Unet_s, a_real_Unet_s,
                                                            gt_real_single_peaks, Unet_s_real_single_peaks)
            mae_real[4, p_idx] += single_peaks_angular_error(dir_real_gt, a_real_gt,
                                                            dir_real_Unet, a_real_Unet,
                                                            gt_real_single_peaks, Unet_real_single_peaks)

            mae_syn1[0, p_idx] += single_peaks_angular_error(dir_syn1_gt, a_syn1_gt,
                                                            dir_syn1_msmt, a_syn1_msmt,
                                                            gt_syn1_single_peaks, msmt_syn1_single_peaks)
            mae_syn1[1, p_idx] += single_peaks_angular_error(dir_syn1_gt, a_syn1_gt,
                                                            dir_syn1_3dcnn, a_syn1_3dcnn,
                                                            gt_syn1_single_peaks, cnn_syn1_single_peaks)
            mae_syn1[2, p_idx] += single_peaks_angular_error(dir_syn1_gt, a_syn1_gt,
                                                            dir_syn1_Unet_1vox, a_syn1_Unet_1vox,
                                                            gt_syn1_single_peaks, Unet_1vox_syn1_single_peaks)
            mae_syn1[3, p_idx] += single_peaks_angular_error(dir_syn1_gt, a_syn1_gt,
                                                            dir_syn1_Unet_s, a_syn1_Unet_s,
                                                            gt_syn1_single_peaks, Unet_s_syn1_single_peaks)
            mae_syn1[4, p_idx] += single_peaks_angular_error(dir_syn1_gt, a_syn1_gt,
                                                            dir_syn1_Unet, a_syn1_Unet,
                                                            gt_syn1_single_peaks, Unet_syn1_single_peaks)

            mae_syn2[0, p_idx] += single_peaks_angular_error(dir_syn2_gt, a_syn2_gt,
                                                            dir_syn2_msmt, a_syn2_msmt,
                                                            gt_syn2_single_peaks, msmt_syn2_single_peaks)
            mae_syn2[1, p_idx] += single_peaks_angular_error(dir_syn2_gt, a_syn2_gt,
                                                            dir_syn2_3dcnn, a_syn2_3dcnn,
                                                            gt_syn2_single_peaks, cnn_syn2_single_peaks)
            mae_syn2[2, p_idx] += single_peaks_angular_error(dir_syn2_gt, a_syn2_gt,
                                                            dir_syn2_Unet_1vox, a_syn2_Unet_1vox,
                                                            gt_syn2_single_peaks, Unet_1vox_syn2_single_peaks)
            mae_syn2[3, p_idx] += single_peaks_angular_error(dir_syn2_gt, a_syn2_gt,
                                                            dir_syn2_Unet_s, a_syn2_Unet_s,
                                                            gt_syn2_single_peaks, Unet_s_syn2_single_peaks)
            mae_syn2[4, p_idx] += single_peaks_angular_error(dir_syn2_gt, a_syn2_gt,
                                                            dir_syn2_Unet, a_syn2_Unet,
                                                            gt_syn2_single_peaks, Unet_syn2_single_peaks)

    mae_real /= 10
    mae_syn1 /= 10
    mae_syn2 /= 10

    points = [20, 30, 40, 60, 90, 120]
    fig, ax = plt.subplots(nrows=1, ncols=3)
    ax[0].plot(points, mae_real[0, :], linestyle='-', marker='o', color='royalblue', linewidth=3)
    ax[0].plot(points, mae_real[1, :], linestyle='-', marker='o', color='orangered', linewidth=3)
    ax[0].plot(points, mae_real[2, :], linestyle='-', marker='o', color='blueviolet', linewidth=3)
    ax[0].plot(points, mae_real[3, :], linestyle='-', marker='o', color='yellowgreen', linewidth=3)
    ax[0].plot(points, mae_real[4, :], linestyle='-', marker='o', color='k', linewidth=3)
    ax[0].set_xlabel('$N_{DWI}$', fontsize=16)
    ax[0].set_ylabel('MAE', fontsize=16)
    ax[0].set_ylim([1, 6.5])
    ax[0].set_xticks(points)
    ax[0].set_title('Real dataset', fontsize=16)
    ax[0].grid()

    ax[1].plot(points, mae_syn1[0, :], linestyle='-', marker='o', color='royalblue', linewidth=3)
    ax[1].plot(points, mae_syn1[1, :], linestyle='-', marker='o', color='orangered', linewidth=3)
    ax[1].plot(points, mae_syn1[2, :], linestyle='-', marker='o', color='blueviolet', linewidth=3)
    ax[1].plot(points, mae_syn1[3, :], linestyle='-', marker='o', color='yellowgreen', linewidth=3)
    ax[1].plot(points, mae_syn1[4, :], linestyle='-', marker='o', color='k', linewidth=3)
    ax[1].set_xlabel('$N_{DWI}$', fontsize=16)
    ax[1].set_ylim([1, 6.5])
    ax[1].set_yticklabels([])
    ax[1].set_xticks(points)
    ax[1].set_title('Synthetic dataset 1', fontsize=16)
    ax[1].grid()

    ax[2].plot(points, mae_syn2[0, :], linestyle='-', marker='o', color='royalblue', linewidth=3)
    ax[2].plot(points, mae_syn2[1, :], linestyle='-', marker='o', color='orangered', linewidth=3)
    ax[2].plot(points, mae_syn2[2, :], linestyle='-', marker='o', color='blueviolet', linewidth=3)
    ax[2].plot(points, mae_syn2[3, :], linestyle='-', marker='o', color='yellowgreen', linewidth=3)
    ax[2].plot(points, mae_syn2[4, :], linestyle='-', marker='o', color='k', linewidth=3)
    ax[2].set_xlabel('$N_{DWI}$', fontsize=16)
    ax[2].set_ylim([1, 6.5])
    ax[2].set_yticklabels([])
    ax[2].set_xticks(points)
    ax[2].grid()
    ax[2].set_title('Synthetic dataset 2', fontsize=16)
    ax[2].legend(['MSMT-CSD', '3DCNN', '$S^2Unet^{1x1x1}$', '$S^2Unet^{3x3x3}_s$', '$S^2Unet^{3x3x3}$'], loc="upper right", fontsize=16)
    plt.show()


if __name__ == '__main__':
    main()