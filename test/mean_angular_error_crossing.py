import os
import numpy as np
import nibabel as nib


def peaks_process(peaks_in):

    peaks = np.zeros((peaks_in.shape[0], 3, 3))

    peaks[:, 0, :] = peaks_in[:, 0:3]
    peaks[:, 1, :] = peaks_in[:, 3:6]
    peaks[:, 2, :] = peaks_in[:, 6:9]

    peaks_amp = np.sqrt(np.sum(peaks**2, axis=2))

    return peaks, peaks_amp

def two_peaks_mask(peaks_a):

    two_peaks_mask = (0.1 * peaks_a[:, 0] < peaks_a[:, 1]) * (0.1 * peaks_a[:, 0] > peaks_a[:, 2])

    return two_peaks_mask

def two_peaks_angular_error(gt_dir, gt_a, comp_dir, comp_a, mask_gt, mask_comp):

        mask = mask_gt * mask_comp

        cosine_sim_00 = np.sum(gt_dir[mask, 0, :] * comp_dir[mask, 0, :], axis=1)/(gt_a[mask, 0] * comp_a[mask, 0])
        cosine_sim_01 = np.sum(gt_dir[mask, 0, :] * comp_dir[mask, 1, :], axis=1)/(gt_a[mask, 0] * comp_a[mask, 1])
        cosine_sim_10 = np.sum(gt_dir[mask, 1, :] * comp_dir[mask, 0, :], axis=1)/(gt_a[mask, 1] * comp_a[mask, 0])
        cosine_sim_11 = np.sum(gt_dir[mask, 1, :] * comp_dir[mask, 1, :], axis=1)/(gt_a[mask, 1] * comp_a[mask, 1])

        angular_error_00 = np.abs(np.arccos(cosine_sim_00)) * 180/np.pi
        conv_180 = angular_error_00 > 90
        angular_error_00[conv_180] = 180 - angular_error_00[conv_180]

        angular_error_01 = np.abs(np.arccos(cosine_sim_01)) * 180/np.pi
        conv_180 = angular_error_00 > 90
        angular_error_01[conv_180] = 180 - angular_error_01[conv_180]

        angular_error_10 = np.abs(np.arccos(cosine_sim_10)) * 180/np.pi
        conv_180 = angular_error_00 > 90
        angular_error_10[conv_180] = 180 - angular_error_10[conv_180]

        angular_error_11 = np.abs(np.arccos(cosine_sim_11)) * 180/np.pi
        conv_180 = angular_error_00 > 90
        angular_error_11[conv_180] = 180 - angular_error_11[conv_180]

        angular_errors_all = np.zeros((angular_error_00.shape[0], 2))
        angular_errors_all[:, 0] = angular_error_00 + angular_error_11
        angular_errors_all[:, 1] = angular_error_01 + angular_error_10

        angular_error = np.min(angular_errors_all, axis=1)
        return np.mean(angular_error), np.std(angular_error)


def main():

    subjects = sorted(os.listdir('/user/ssedlar/home/Work/exp_out/CDMRI/Real/3DCNN/20/test_200'))
    gt_template_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/ground_truth/{}/peaks.nii.gz'
    mask_template_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/ground_truth/{}/mask.npy'

    msmt_template_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/MSMT_CSD/{}/{}/peaks.nii.gz'
    cnn_template_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/3DCNN/{}/test_200/{}/peaks.nii.gz'
    unet_template_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/Unet/{}/test_200/{}/peaks.nii.gz'
    unet_vox_template_path = '/user/ssedlar/home/Work/exp_out/CDMRI/Real/Unet_1vox/{}/test_200/{}/peaks.nii.gz'

    ae_mean = np.zeros((4, 6))
    ae_std = np.zeros((4, 6))

    for p_idx, p in enumerate([20, 30, 40, 60, 90, 120]):
        print("p:", p)
        for s in subjects:
            print(s)
            mask = np.load(mask_template_path.format(s))
            gt_peaks = nib.load(gt_template_path.format(s)).get_data()[mask==1, :]

            msmt_peaks = nib.load(msmt_template_path.format(p, s)).get_data()[mask==1, :]
            cnn_peaks = nib.load(cnn_template_path.format(p, s)).get_data()[mask==1, :]
            unet_peaks = nib.load(unet_template_path.format(p, s)).get_data()[mask==1, :]
            unet_vox_peaks = nib.load(unet_vox_template_path.format(p, s)).get_data()[mask==1, :]

            gt_peaks_d, gt_peaks_a = peaks_process(gt_peaks)
            msmt_peaks_d, msmt_peaks_a = peaks_process(msmt_peaks)
            cnn_peaks_d, cnn_peaks_a = peaks_process(cnn_peaks)
            unet_peaks_d, unet_peaks_a = peaks_process(unet_peaks)
            unet_vox_peaks_d, unet_vox_peaks_a = peaks_process(unet_vox_peaks)

            gt_two_peaks = two_peaks_mask(gt_peaks_a)
            msmt_two_peaks = two_peaks_mask(cnn_peaks_a)
            cnn_two_peaks = two_peaks_mask(cnn_peaks_a)
            unet_two_peaks = two_peaks_mask(unet_peaks_a)
            unet_vox_two_peaks = two_peaks_mask(unet_vox_peaks_a)

            msmt_two_ae_mean, msmt_two_ae_std = two_peaks_angular_error(gt_peaks_d, gt_peaks_a, msmt_peaks_d, msmt_peaks_a,
                                                        gt_two_peaks, msmt_two_peaks)

            cnn_two_ae_mean, cnn_two_ae_std = two_peaks_angular_error(gt_peaks_d, gt_peaks_a, cnn_peaks_d, cnn_peaks_a,
                                                        gt_two_peaks, cnn_two_peaks)

            unet_two_ae_mean, unet_two_ae_std = two_peaks_angular_error(gt_peaks_d, gt_peaks_a, unet_peaks_d, unet_peaks_a,
                                                        gt_two_peaks, unet_two_peaks)

            unet_vox_two_ae_mean, unet_vox_two_ae_std = two_peaks_angular_error(gt_peaks_d, gt_peaks_a, unet_vox_peaks_d, unet_vox_peaks_a,
                                                        gt_two_peaks, unet_vox_two_peaks)

            ae_mean[0, p_idx] += msmt_two_ae_mean
            ae_std[0, p_idx] += msmt_two_ae_std

            ae_mean[1, p_idx] += cnn_two_ae_mean
            ae_std[1, p_idx] += cnn_two_ae_std

            ae_mean[2, p_idx] += unet_vox_two_ae_mean
            ae_std[2, p_idx] += unet_vox_two_ae_std

            ae_mean[3, p_idx] += unet_two_ae_mean
            ae_std[3, p_idx] += unet_two_ae_std

    ae_mean /= 10
    ae_std /= 10

    x = np.arange(0, 6)

    import matplotlib.pyplot as plt
    plt.figure(1)
    plt.plot(x, ae_mean[0, :], color='r')
    plt.plot(x, ae_mean[1, :], color='g')
    plt.plot(x, ae_mean[2, :], color='b')
    plt.plot(x, ae_mean[3, :], color='k')
    plt.ylabel('Mean angular error [degrees]')
    plt.xlabel('Number of dMRI gradient directions (sampling points)')
    plt.xticks(x, [20, 30, 40, 60, 90, 120])
    plt.grid()
    plt.legend(['MSMT_CSD', '3DCNN', 'sphUnet_1x1x1', 'sphUnet_3x3x3'])
    plt.show()

if __name__ == '__main__':
    main()